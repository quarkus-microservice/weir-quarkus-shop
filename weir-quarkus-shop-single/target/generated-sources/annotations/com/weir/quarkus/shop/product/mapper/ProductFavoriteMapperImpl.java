package com.weir.quarkus.shop.product.mapper;

import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.vo.ProductFavoriteVo;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-02-18T16:15:11+0800",
    comments = "version: 1.6.0.Beta1, compiler: javac, environment: Java 21.0.1 (Oracle Corporation)"
)
@Singleton
@Named
public class ProductFavoriteMapperImpl implements ProductFavoriteMapper {

    @Override
    public ProductFavoriteVo toVo(ProductSpu spu) {
        if ( spu == null ) {
            return null;
        }

        ProductFavoriteVo productFavoriteVo = new ProductFavoriteVo();

        return productFavoriteVo;
    }
}
