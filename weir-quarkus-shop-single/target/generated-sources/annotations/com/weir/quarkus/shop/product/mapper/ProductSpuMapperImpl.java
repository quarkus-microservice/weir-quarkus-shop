package com.weir.quarkus.shop.product.mapper;

import com.weir.quarkus.shop.product.entity.ProductSku;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.vo.ProductSkuVo;
import com.weir.quarkus.shop.product.vo.ProductSpuVo;
import jakarta.inject.Named;
import jakarta.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-02-18T16:15:11+0800",
    comments = "version: 1.6.0.Beta1, compiler: javac, environment: Java 21.0.1 (Oracle Corporation)"
)
@Singleton
@Named
public class ProductSpuMapperImpl implements ProductSpuMapper {

    @Override
    public ProductSpu toSpuEntity(ProductSpuVo spu) {
        if ( spu == null ) {
            return null;
        }

        ProductSpu productSpu = new ProductSpu();

        return productSpu;
    }

    @Override
    public ProductSku toSkuEntity(ProductSkuVo sku) {
        if ( sku == null ) {
            return null;
        }

        ProductSku productSku = new ProductSku();

        return productSku;
    }

    @Override
    public ProductSpuVo toSpuVo(ProductSpu spu) {
        if ( spu == null ) {
            return null;
        }

        ProductSpuVo productSpuVo = new ProductSpuVo();

        return productSpuVo;
    }

    @Override
    public ProductSkuVo toSkuVo(ProductSku sku) {
        if ( sku == null ) {
            return null;
        }

        ProductSkuVo productSkuVo = new ProductSkuVo();

        return productSkuVo;
    }

    @Override
    public List<ProductSkuVo> toSkuVo(List<ProductSku> skus) {
        if ( skus == null ) {
            return null;
        }

        List<ProductSkuVo> list = new ArrayList<ProductSkuVo>( skus.size() );
        for ( ProductSku productSku : skus ) {
            list.add( toSkuVo( productSku ) );
        }

        return list;
    }

    @Override
    public List<ProductSpuVo> toSpuVo(List<ProductSpu> spus) {
        if ( spus == null ) {
            return null;
        }

        List<ProductSpuVo> list = new ArrayList<ProductSpuVo>( spus.size() );
        for ( ProductSpu productSpu : spus ) {
            list.add( toSpuVo( productSpu ) );
        }

        return list;
    }
}
