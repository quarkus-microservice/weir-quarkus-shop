package com.weir.quarkus.config;

import com.querydsl.jpa.impl.JPAQueryFactory;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import jakarta.persistence.EntityManager;

public class QueryDslConfig {
  @Inject EntityManager entityManager;

  @Singleton
  public JPAQueryFactory jpaQueryFactory() {
    return new JPAQueryFactory(entityManager);
  }
}