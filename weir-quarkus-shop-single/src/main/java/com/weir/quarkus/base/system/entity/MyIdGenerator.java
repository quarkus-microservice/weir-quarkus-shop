package com.weir.quarkus.base.system.entity;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

import com.weir.quarkus.base.system.utils.Sequence;

import java.io.Serializable;

/**
 * 雪花算法生产id(分布式高效有序 ID )
 * @author weir
 * @date 2023-03-30 11:13:34
 */
public class MyIdGenerator implements IdentifierGenerator {
	private static final long serialVersionUID = -3143346991662178588L;
	private final Sequence sequence = new Sequence(null);
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) throws HibernateException {
        return sequence.nextId();
    }
}
