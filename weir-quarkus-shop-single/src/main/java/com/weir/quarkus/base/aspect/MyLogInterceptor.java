package com.weir.quarkus.base.aspect;

import java.util.Arrays;

import jakarta.interceptor.AroundInvoke;
import jakarta.interceptor.Interceptor;
import jakarta.interceptor.InvocationContext;

@Interceptor //标识这个是拦截器
@AutoLog //需要匹配的注解
class MyLogInterceptor{
    //编写环绕通知逻辑
    @AroundInvoke
    Object markInvocation(InvocationContext context) {
        //context包含调用点信息比如method paramter 之类的
        System.out.println("---------------------before");
        System.out.println("---------------------getMethod-----" + context.getMethod());
        System.out.println("---------------------getParameters----" + Arrays.deepToString(context.getParameters()));
        System.out.println("---------------------getTarget----" + context.getTarget());
        System.out.println("---------------------getTimer-----" + context.getTimer());
        try {
        	System.out.println("---------------------proceed-------" + context.proceed());
			return context.proceed();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        System.out.println("---------------------after");
        return null;
    }
}
