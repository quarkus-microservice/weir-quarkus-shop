//package com.weir.quarkus.base.system.jwt;
//
//import org.eclipse.microprofile.jwt.Claims;
//import org.jboss.logmanager.Logger;
//import org.jose4j.jwt.JwtClaims;
//
//import com.weir.quarkus.base.system.utils.TokenUtils;
//
//import jakarta.enterprise.context.RequestScoped;
//import java.util.Set;
//import java.util.UUID;
//
//@RequestScoped
//public class TokenService {
//
//    public final static Logger LOGGER = Logger.getLogger(TokenService.class.getSimpleName());
//
//    public String generateUserToken(String email, String username, Set<String> group) {
//        return generateToken(email, username, group);
//    }
//
//    public String generateToken(String subject, String name, Set<String> group) {
//        try {
//            JwtClaims jwtClaims = new JwtClaims();
//            jwtClaims.setIssuer("http://www.loveweir.com"); // change to your company
//            jwtClaims.setJwtId(UUID.randomUUID().toString());
//            jwtClaims.setSubject(subject);
//            jwtClaims.setClaim(Claims.upn.name(), subject);
//            jwtClaims.setClaim(Claims.preferred_username.name(), name); //add more
//            jwtClaims.setClaim(Claims.groups.name(), group);
//            jwtClaims.setAudience("using-jwt");
//            jwtClaims.setExpirationTimeMinutesInTheFuture(86400); // TODO specify how long do you need
//
//
//            String token = TokenUtils.generateTokenString(jwtClaims);
//            LOGGER.info("TOKEN generated: " + token);
//            return token;
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new RuntimeException(e);
//        }
//    }
//}
