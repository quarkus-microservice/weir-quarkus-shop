package com.weir.quarkus.base.aspect;

import lombok.Data;

@Data
public class DictVo {

	/**
	 * 字典value
	 */
	private String value;
	/**
	 * 字典文本
	 */
	private String text;
	
}
