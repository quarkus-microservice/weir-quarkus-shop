package com.weir.quarkus.shop.utils;

import jakarta.persistence.TypedQuery;

import com.weir.quarkus.shop.vo.VuePageListVo;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.hibernate.orm.panache.PanacheQuery;

/**
 * 
 * @Title: VuePageUtil.java 
 * @Package com.weir.quarkus.shop.utils 
 * @Description: 分页数据计算组装
 * @author weir
 * @date 2023年12月12日 15:04:16 
 * @version V1.0
 */
@SuppressWarnings({ "rawtypes", "unchecked" })
public class VuePageUtil {

	public static VuePageListVo toPage(PanacheQuery<PanacheEntityBase> panacheQuery, VuePageListVo list, Integer page,
			Integer pageSize) {
		long count = panacheQuery.count();
		list.setPage(page);
		list.setPageSize(pageSize);
		list.setList(panacheQuery.list());
		list.setPageCount(count % pageSize == 0 ? count / pageSize : count / pageSize + 1);
		return list;
	}
	public static VuePageListVo toPageForTypedQuery(TypedQuery typedQuery, VuePageListVo list, Integer page,
			Integer pageSize) {
		Long count = Integer.valueOf(typedQuery.getResultList().size()).longValue();
		list.setPage(page);
		list.setPageSize(pageSize);
		list.setList(typedQuery.setFirstResult(page * pageSize).setMaxResults(pageSize).getResultList());
		list.setPageCount(count % pageSize == 0 ? count / pageSize : count / pageSize + 1);
		return list;
	}
	public static VuePageListVo toListForTypedQuery(TypedQuery typedQuery, VuePageListVo list) {
		list.setList(typedQuery.getResultList());
		return list;
	}
}
