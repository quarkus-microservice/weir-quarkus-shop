package com.weir.quarkus.shop.marketing.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.marketing.entity.Article;
import com.weir.quarkus.shop.marketing.entity.ArticleCategory;
import com.weir.quarkus.shop.marketing.vo.ArticleCategoryQuery;
import com.weir.quarkus.shop.product.entity.ProductBrand;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

import io.quarkus.panache.common.Sort;

@Tag(name = "管理后台 - 文章分类")
@Path("/promotion/article-category")
public class ArticleCategoryResource {
	@Inject
	EntityManager em;

    @POST
    @Path("/create")
    @Operation(summary = "创建文章分类")
//    @PreAuthorize("@ss.hasPermission('promotion:article-category:create')")
    @Transactional
    public Response createArticleCategory(@Valid @RequestBody ArticleCategory articleCategory) {
    	articleCategory.persist();
		return Response.ok().entity(new ResultDataVo<>(200, "创建文章分类成功" , articleCategory)).build();
    }

    @PUT
    @Path("/update")
    @Operation(summary = "更新文章分类")
//    @PreAuthorize("@ss.hasPermission('promotion:article-category:update')")
    @Transactional
    public Response updateArticleCategory(@Valid @RequestBody ArticleCategory articleCategory) {
    	if (articleCategory.getId() == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "文章分类ID不能为空")).build();
		}
    	ArticleCategory old = ArticleCategory.findById(articleCategory.getId());
    	if (old == null) {			
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "文章分类不存在")).build();
		}
//    	old.setName(articleCategory.getName());
//    	old.setPicUrl(articleCategory.getPicUrl());
//    	old.setSort(articleCategory.getSort());
//    	old.setStatus(articleCategory.getStatus());
    	articleCategory.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "更新文章分类成功" , old)).build();
    }

    @DELETE
    @Path("/delete")
    @Operation(summary = "删除文章分类")
    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:article-category:delete')")
    @Transactional
    public Response deleteArticleCategory(@QueryParam("id") Integer id) {
    	long count = Article.count("categoryId", id);
    	if (count > 0) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "文章分类不能删除，存在关联文章")).build();
		}
    	Article.deleteById(id);
    	return Response.ok().entity(new ResultDataVo<>(200, "更新文章分类成功" , id)).build();
    }

    @GET
    @Path("/get")
    @Operation(summary = "获得文章分类")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('promotion:article-category:query')")
    public Response getArticleCategory(@QueryParam("id") Integer id) {
    	return Response.ok().entity(new ResultDataVo<>(200, "更新文章分类成功" , ArticleCategory.findById(id))).build();
    }

    @GET
    @Path("/list-all-simple")
    @Operation(summary = "获取文章分类精简信息列表", description = "只包含被开启的文章分类，主要用于前端的下拉选项")
    public Response getSimpleDeptList() {
//        // 获得分类列表，只要开启状态的
//        List<ArticleCategory> list = articleCategoryService.getArticleCategoryListByStatus(CommonStatusEnum.ENABLE.getStatus());
//        // 降序排序后，返回给前端
//        list.sort(Comparator.comparing(ArticleCategory::getSort).reversed());
//        return success(ArticleCategoryConvert.INSTANCE.convertList03(list));
    	List<ArticleCategory> list = ArticleCategory.list("status=?1", Sort.by("sort"),0);
    	return Response.ok().entity(new ResultDataVo<>(200, "更新文章分类成功" , list)).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得文章分类分页")
//    @PreAuthorize("@ss.hasPermission('promotion:article-category:query')")
    public Response getArticleCategoryPage(@Valid ArticleCategoryQuery query) {
    	VuePageListVo<ArticleCategory> queryPage = QueryHelper.createQueryPage(em, ArticleCategory.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPage)).build();
    }

}
