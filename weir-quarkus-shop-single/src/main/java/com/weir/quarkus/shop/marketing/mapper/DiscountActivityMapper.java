package com.weir.quarkus.shop.marketing.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.weir.quarkus.shop.marketing.entity.DiscountActivity;
import com.weir.quarkus.shop.marketing.entity.DiscountProduct;
import com.weir.quarkus.shop.marketing.vo.DiscountActivityDTO;
import com.weir.quarkus.shop.marketing.vo.DiscountProductDTO;

/** 
 * @Title: ProductBrandMapper.java 
 * @Package com.weir.quarkus.shop.product.mapper 
 * @Description: 收藏夹
 * @author weir
 * @date 2023年12月12日 14:12:05 
 * @version V1.0 
 */
@Mapper(componentModel = "jakarta", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface DiscountActivityMapper {

	DiscountActivity toEntity(DiscountActivityDTO dto);
	DiscountProduct toProductEntity(DiscountProductDTO dto);
	List<DiscountProduct> toProductEntity(List<DiscountProductDTO> dto);
	
	DiscountActivityDTO toDTO(DiscountActivity da);
	List<DiscountActivityDTO> toDTO(List<DiscountActivity> das);
	
	DiscountProductDTO toProductDTO(DiscountProduct dp);
	List<DiscountProductDTO> toProductDTO(List<DiscountProduct> dps);
}
