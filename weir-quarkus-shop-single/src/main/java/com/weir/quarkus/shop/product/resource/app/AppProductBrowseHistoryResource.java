package com.weir.quarkus.shop.product.resource.app;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductBrowseHistory;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.mapper.ProductBrowseHistoryMapper;
import com.weir.quarkus.shop.product.vo.ProductBrowseHistoryVo;
import com.weir.quarkus.shop.product.vo.app.AppProductBrowseHistoryDeleteReqVO;
import com.weir.quarkus.shop.product.vo.app.ProductBrowseHistoryQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "用户 APP - 商品浏览记录")
@Path("app/product/browse-history")
public class AppProductBrowseHistoryResource {

	@Inject
	EntityManager em;
	@Inject
	ProductBrowseHistoryMapper productBrowseHistoryMapper;
	
    @DELETE
    @Path(value = "/delete")
    @Operation(summary = "删除商品浏览记录")
    public Response deleteBrowseHistory(@RequestBody @Valid AppProductBrowseHistoryDeleteReqVO reqVO) {
    	ProductBrowseHistory.update("userDeleted=true where userId=?1 and spuId in (?2)", null, reqVO.getSpuIds());
    	return Response.ok().entity(new ResultDataVo<>(200, "删除商品浏览记录成功", true)).build();
    }

    @DELETE
    @Path(value = "/clean")
    @Operation(summary = "清空商品浏览记录")
    public Response deleteBrowseHistory() {
    	ProductBrowseHistory.update("userDeleted=true where userId=?1", 11);
    	return Response.ok().entity(new ResultDataVo<>(200, "清空商品浏览记录成功", true)).build();
    }

    @GET
    @Path(value = "/page")
    @Operation(summary = "获得商品浏览记录分页")
    public Response getBrowseHistoryPage(ProductBrowseHistoryQuery query) {
    	query.setUserDeleted(false);
    	query.setUserId(11);
    	VuePageListVo<ProductBrowseHistory> queryPage = QueryHelper.createQueryPage(em, ProductBrowseHistory.class, query);
    	List<ProductBrowseHistory> histories = queryPage.getList();
    	VuePageListVo vuePageListVo = new VuePageListVo<>();
		if (CollectionUtils.isEmpty(histories)) {
			return Response.ok().entity(new ResultDataVo<>(200, "ok", vuePageListVo)).build();
		}
		List<Integer> spuIds = histories.stream().map(ProductBrowseHistory::getSpuId).collect(Collectors.toList());
		List<ProductSpu> spus = ProductSpu.list("id in (?1)", spuIds);
		Map<Integer, ProductSpu> spuIdMap = spus.stream()
				.collect(Collectors.toMap(ProductSpu::getId, a -> a, (k1, k2) -> k1));
		List<ProductBrowseHistoryVo> fVos = new ArrayList<>();
		for (ProductBrowseHistory productFavorite : histories) {
			ProductSpu spu = spuIdMap.get(productFavorite.getSpuId());
			ProductBrowseHistoryVo vo = productBrowseHistoryMapper.toVo(spu);
			vo.setId(productFavorite.getId());
			vo.setUserId(productFavorite.getUserId());
			vo.setSpuId(spu.getId());
			fVos.add(vo);
		}
		
		vuePageListVo.setList(fVos);
		vuePageListVo.setPage(queryPage.getPage());
		vuePageListVo.setPageCount(queryPage.getPageCount());
		vuePageListVo.setPageSize(queryPage.getPageSize());
		vuePageListVo.setTotal(queryPage.getTotal());
		return Response.ok().entity(new ResultDataVo<>(200, "ok", vuePageListVo)).build();
    }

}
