package com.weir.quarkus.shop.product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import com.weir.quarkus.shop.entity.BaseEntity;

import lombok.*;

/**
 * 商品 SPU DO
 *
 * @author 芋道源码
 */
@Entity
@Table(name = "product_spu")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductSpu extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 商品名称
     */
    private String name;
    /**
     * 关键字
     */
    private String keyword;
    /**
     * 商品简介
     */
    private String introduction;
    /**
     * 商品详情
     */
    private String description;
    /**
     * 商品条码（一维码）
     */
    @Column(name = "bar_code")
    private String barCode;

    /**
     * 商品分类编号
     *
     */
    @Column(name = "category_id")
    private Integer categoryId;
    /**
     * 商品品牌编号
     *
     */
    @Column(name = "brand_id")
    private Integer brandId;
    /**
     * 商品封面图
     */
    @Column(name = "pic_url")
    private String picUrl;
    /**
     * 商品轮播图(逗号分隔)
     */
    @Column(name = "slider_pic_urls")
    private String sliderPicUrls;
    /**
     * 商品视频
     */
    @Column(name = "video_url")
    private String videoUrl;

    /**
     * 单位
     *
     */
    private Integer unit;
    /**
     * 排序字段
     */
    private Integer sort;
    /**
     * 商品状态
     *
     */
    private Integer status;

    /**
     * 规格类型
     *
     * false - 单规格
     * true - 多规格
     */
    @Column(name = "spec_type")
    private Boolean specType;
    /**
     * 商品价格，单位使用：分
     *
     */
    private Integer price;
    /**
     * 市场价，单位使用：分
     *
     */
    @Column(name = "market_price")
    private Integer marketPrice;
    /**
     * 成本价，单位使用：分
     *
     */
    @Column(name = "cost_price")
    private Integer costPrice;
    /**
     * 库存
     *
     */
    private Integer stock;

    /**
     * 物流配置模板编号
     *
     */
    @Column(name = "delivery_template_id")
    private Integer deliveryTemplateId;

    /**
     * 是否热卖推荐
     */
    @Column(name = "recommend_hot")
    private Boolean recommendHot;
    /**
     * 是否优惠推荐
     */
    @Column(name = "recommend_benefit")
    private Boolean recommendBenefit;
    /**
     * 是否精品推荐
     */
    @Column(name = "recommend_best")
    private Boolean recommendBest;
    /**
     * 是否新品推荐
     */
    @Column(name = "recommend_new")
    private Boolean recommendNew;
    /**
     * 是否优品推荐
     */
    @Column(name = "recommend_good")
    private Boolean recommendGood;

    /**
     * 赠送积分
     */
    @Column(name = "give_integral")
    private Integer giveIntegral;
    /**
     * 赠送的优惠劵编号的数组(逗号分隔)
     *
     */
    @Column(name = "give_coupon_template_ids")
    private String giveCouponTemplateIds;

    /**
     * 分销类型
     *
     * false - 默认
     * true - 自行设置
     */
    @Column(name = "sub_commission_type")
    private Boolean subCommissionType;

    /**
     * 活动展示顺序(逗号分隔)
     *
     */
    @Column(name = "activity_orders")
    private String activityOrders;

    /**
     * 商品销量
     */
    @Column(name = "sales_count")
    private Integer salesCount;
    /**
     * 虚拟销量
     */
    @Column(name = "virtual_sales_count")
    private Integer virtualSalesCount;
    /**
     * 浏览量
     */
    @Column(name = "browse_count")
    private Integer browseCount;
    /**
     * 判断
     * @param categoryId
     * @return
     */
    public static boolean checkCountByCategoryId(Integer categoryId) {
		if (count("categoryId", categoryId) > 0) {
			return true;
		}
		return false;
	}
}
