package com.weir.quarkus.shop.product.resource.app;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.member.entity.MemberLevel;
import com.weir.quarkus.shop.member.entity.MemberUser;
import com.weir.quarkus.shop.product.entity.ProductSku;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.mapper.ProductSpuMapper;
import com.weir.quarkus.shop.product.vo.ProductSkuVo;
import com.weir.quarkus.shop.product.vo.ProductSpuQuery;
import com.weir.quarkus.shop.product.vo.ProductSpuVo;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "用户 APP - 商品 SPU")
@Path("app/product/spu")
public class AppProductSpuResource {

	@Inject
	EntityManager em;
	@Inject
	ProductSpuMapper productSpuMapper;

    @GET
    @Path("/list-by-ids")
    @Operation(summary = "获得商品 SPU 列表")
    @Parameter(name = "ids", description = "编号列表", required = true)
    public Response getSpuList(@QueryParam("ids") Set<Long> ids) {
    	List<ProductSpu> list = ProductSpu.list("id in (?1)", ids);
    	if (CollectionUtils.isEmpty(list)) {
    		return Response.ok().entity(new ResultDataVo<>(200, "ok", CollectionUtils.EMPTY_COLLECTION)).build();
		}
    	List<ProductSpuVo> spuVos = toList(list);
    	return Response.ok().entity(new ResultDataVo<>(200, "ok", spuVos)).build();
    }

	private List<ProductSpuVo> toList(List<ProductSpu> list) {
		list.forEach(spu -> spu.setSalesCount(spu.getSalesCount() + spu.getVirtualSalesCount()));
    	List<ProductSpuVo> spuVos = productSpuMapper.toSpuVo(list);
    	MemberUser memberUser = MemberUser.findById(11);
    	MemberLevel memberLevel = MemberLevel.findById(memberUser.getLevelId());
    	spuVos.forEach(vo->{
    		Integer price = vo.getPrice();
    		if (memberLevel != null && memberLevel.getDiscountPercent() != null) {
    			Integer newPrice = price * memberLevel.getDiscountPercent() / 100;
    			vo.setVipPrice(price - newPrice);
    		}
    	});
		return spuVos;
	}

    @GET
    @Path("/page")
    @Operation(summary = "获得商品 SPU 分页")
    public Response getSpuPage(@BeanParam ProductSpuQuery query) {
    	VuePageListVo<ProductSpu> queryPage = QueryHelper.createQueryPage(em, ProductSpu.class, query);
    	List<ProductSpu> list = queryPage.getList();
    	List<ProductSpuVo> vos = toList(list);
    	
    	VuePageListVo voPage = new VuePageListVo<>();
    	voPage.setList(vos);
    	voPage.setPage(queryPage.getPage());
    	voPage.setPageCount(queryPage.getPageCount());
    	voPage.setPageSize(queryPage.getPageSize());
    	voPage.setTotal(queryPage.getTotal());
    	
		return Response.ok().entity(new ResultDataVo<>(200, voPage)).build();
    }

    @GET
    @Path("/get-detail")
    @Operation(summary = "获得商品 SPU 明细")
    @Parameter(name = "id", description = "编号", required = true)
    public Response getSpuDetail(@QueryParam("id") Long id) {
    	ProductSpu spu = ProductSpu.findById(id);
    	if (spu == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SPU 不存在")).build();
		}
    	if (!spu.getStatus().equals(1)) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SPU 不处于上架状态")).build();
		}
    	List<ProductSku> skus = ProductSku.list("spuId=?1", id);
    	// 增加浏览量
    	// 保存浏览记录
    	spu.setBrowseCount(spu.getBrowseCount() + spu.getVirtualSalesCount());
    	ProductSpuVo spuVo = productSpuMapper.toSpuVo(spu);
    	List<ProductSkuVo> skuVos = productSpuMapper.toSkuVo(skus);
    	spuVo.setSkus(skuVos);
    	
    	// 处理 vip 价格
    	MemberUser memberUser = MemberUser.findById(11);
    	MemberLevel memberLevel = MemberLevel.findById(memberUser.getLevelId());
    	Integer price = spuVo.getPrice();
		if (memberLevel != null && memberLevel.getDiscountPercent() != null) {
			Integer newPrice = price * memberLevel.getDiscountPercent() / 100;
			spuVo.setVipPrice(price - newPrice);
		}
		return Response.ok().entity(new ResultDataVo<>(200, spuVo)).build();
    }

}
