package com.weir.quarkus.shop.product.resource.admin;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductPropertyValue;
import com.weir.quarkus.shop.product.vo.ProductPropertyValueQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Tag(name = "管理后台 - 商品属性值")
@Path("/product/property/value")
public class ProductPropertyValueResource {

	@Inject
	EntityManager em;
	
    @POST
    @Path("/create")
    @Operation(summary = "创建属性值")
//    @PreAuthorize("@ss.hasPermission('product:property:create')")
    @Transactional
    public Response createPropertyValue(ProductPropertyValue propertyValue) {
    	ProductPropertyValue pv = ProductPropertyValue.find("propertyId = ?1 and name = ?2", propertyValue.getPropertyId(), propertyValue.getName()).singleResult();
    	if (pv != null) {
    		return Response.ok().entity(new ResultDataVo<>(200 , pv)).build();
		}
    	propertyValue.persist();
    	return Response.ok().entity(new ResultDataVo<>(200 , propertyValue)).build();
    }

    @PUT
    @Path("/update")
    @Operation(summary = "更新属性值")
//    @PreAuthorize("@ss.hasPermission('product:property:update')")
    @Transactional
    public Response updatePropertyValue(ProductPropertyValue propertyValue) {
    	ProductPropertyValue pv = ProductPropertyValue.findById(propertyValue.getId());
    	if (pv == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该属性值不存在")).build();
		}
    	if (!propertyValue.getName().equals(pv.getName())) {
    		long c = ProductPropertyValue.count("propertyId = ?1 and name = ?2", propertyValue.getPropertyId(), propertyValue.getName());
    		if (c > 0) {
    			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该属性值已存在")).build();
			}
		}
    	pv.setName(propertyValue.getName());
    	pv.setRemark(propertyValue.getRemark());
    	return Response.ok().entity(new ResultDataVo<>(200 , pv)).build();
    }

    @DELETE
    @Path("/delete")
    @Operation(summary = "删除属性值")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('product:property:delete')")
    public Response deletePropertyValue(@QueryParam("id") Integer id) {
    	ProductPropertyValue pv = ProductPropertyValue.findById(id);
    	if (pv == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该属性值不存在")).build();
		}
    	ProductPropertyValue.deleteById(id);
    	return Response.ok().entity(new ResultDataVo<>(200 , "删除属性值成功")).build();
    }

    @GET
    @Path("/get")
    @Operation(summary = "获得属性值")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('product:property:query')")
    public Response getPropertyValue(@QueryParam("id") Integer id) {
    	return Response.ok().entity(new ResultDataVo<>(200 , ProductPropertyValue.findById(id))).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得属性值分页")
//    @PreAuthorize("@ss.hasPermission('product:property:query')")
    public Response getPropertyValuePage(@BeanParam ProductPropertyValueQuery query) {
    	VuePageListVo<ProductPropertyValue> queryPage = QueryHelper.createQueryPage(em, ProductPropertyValue.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, queryPage)).build();
    }
}
