package com.weir.quarkus.shop.marketing.resource.admin;


import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.marketing.entity.Coupon;
import com.weir.quarkus.shop.marketing.entity.CouponTemplate;
import com.weir.quarkus.shop.marketing.enums.CouponStatusEnum;
import com.weir.quarkus.shop.marketing.enums.CouponTakeTypeEnum;
import com.weir.quarkus.shop.marketing.enums.CouponTemplateValidityTypeEnum;
import com.weir.quarkus.shop.marketing.mapper.CouponMapper;
import com.weir.quarkus.shop.marketing.vo.CouponDTO;
import com.weir.quarkus.shop.marketing.vo.CouponQuery;
import com.weir.quarkus.shop.marketing.vo.CouponSendDTO;
import com.weir.quarkus.shop.member.entity.MemberUser;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 优惠劵")
@Path("/promotion/coupon")
public class CouponResource {
	@Inject EntityManager em;
	@Inject CouponMapper couponMapper;

    @DELETE
    @Path("/delete")
    @Operation(summary = "回收优惠劵")
    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:coupon:delete')")
    public Response deleteCoupon(@QueryParam("id") Integer id) {
    	Coupon c = Coupon.findById(id);
    	if (c == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "优惠劵不存在")).build();
		}
    	if (c.getStatus() != 1) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "回收优惠劵失败，优惠劵已被使用")).build();
		}
    	c.delete();
    	return Response.ok().entity(new ResultDataVo<>(200, "回收优惠劵成功", id)).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得优惠劵分页")
//    @PreAuthorize("@ss.hasPermission('promotion:coupon:query')")
    public Response getCouponPage(@Valid CouponQuery query) {
    	VuePageListVo<Coupon> queryPage = QueryHelper.createQueryPage(em, Coupon.class, query);
    	List<Coupon> list = queryPage.getList();
    	List<CouponDTO> dtos = couponMapper.toDTO(list);
    	List<Integer> userIds = dtos.stream().map(CouponDTO::getUserId).distinct().collect(Collectors.toList());
    	List<MemberUser> userList = MemberUser.list("id in (?1)", userIds);
    	Map<Integer, MemberUser> userMap = userList.stream().collect(Collectors.toMap(MemberUser::getId, a -> a, (k1, k2) -> k1));
    	dtos.forEach(c->{
    		MemberUser memberUser = userMap.get(c.getUserId());
    		if (memberUser != null) {
				c.setNickname(memberUser.getNickname());
			}
    	});
    	VuePageListVo<CouponDTO> queryPageVo = new VuePageListVo<>(dtos,queryPage.getPage(),queryPage.getPageSize(),queryPage.getPageCount(),queryPage.getTotal());
		return Response.ok().entity(new ResultDataVo<>(200, queryPageVo)).build();
    }

    @POST
    @Path("/send")
    @Operation(summary = "发送优惠劵")
//    @PreAuthorize("@ss.hasPermission('promotion:coupon:send')")
    public Response sendCoupon(CouponSendDTO dto) {
    	Set<Integer> userIds = dto.getUserIds();
    	CouponTemplate ct = CouponTemplate.findById(dto.getTemplateId());
    	if (ct == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "优惠劵模版不存在")).build();
		}
    	// 校验剩余数量
        if (ct.getTakeCount() + userIds.size() > ct.getTotalCount()) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "当前剩余数量不够领取")).build();
        }
        // 校验"固定日期"的有效期类型是否过期
        if (CouponTemplateValidityTypeEnum.DATE.getType().equals(ct.getValidityType())) {
            if (LocalDateTime.now().isAfter(ct.getValidEndTime())) {
            	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "优惠券已过期")).build();
            }
        }
        if (!ct.getTakeType().equals(CouponTakeTypeEnum.ADMIN.getValue())) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "领取方式不正确")).build();
		}
    	if (ct.getTakeLimitCount() > 0) {			
    		List<Coupon> oldList = Coupon.list("templateId = ?1 and userId in (?2)", dto.getTemplateId(), dto.getUserIds());
    		if (CollectionUtils.isNotEmpty(oldList)) {
    			Map<Integer, Long> userIdCountMap = oldList.stream().collect(Collectors.groupingBy(Coupon::getUserId, Collectors.counting()));
    			userIds.removeIf(userId->{
    				Long c = userIdCountMap.get(userId);
    				return c >= ct.getTakeLimitCount();
    			});
    		}
		}
    	if (CollectionUtils.isEmpty(userIds)) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "用户已领取过此优惠券")).build();
		}
    	
    	for (Integer userId : userIds) {
    		Coupon c = new Coupon();
    		c.setDiscountLimitPrice(ct.getDiscountLimitPrice());
    		c.setDiscountPercent(ct.getDiscountPercent());
    		c.setDiscountPrice(ct.getDiscountPrice());
    		c.setDiscountType(ct.getDiscountType());
    		c.setName(ct.getName());
    		c.setProductScope(ct.getProductScope());
    		c.setProductScopeValues(ct.getProductScopeValues());
    		c.setStatus(CouponStatusEnum.UNUSED.getStatus());
    		c.setTakeType(ct.getTakeType());
    		c.setTemplateId(ct.getId());
    		c.setUsePrice(ct.getUsePrice());
    		c.setUserId(userId);
    		if (CouponTemplateValidityTypeEnum.DATE.getType().equals(ct.getValidityType())) {
                c.setValidStartTime(ct.getValidStartTime());
                c.setValidEndTime(ct.getValidEndTime());
            } else if (CouponTemplateValidityTypeEnum.TERM.getType().equals(ct.getValidityType())) {
                c.setValidStartTime(LocalDateTime.now().plusDays(ct.getFixedStartTerm()));
                c.setValidEndTime(LocalDateTime.now().plusDays(ct.getFixedEndTerm()));
            }
    		c.persist();
		}
    	ct.setTakeCount(ct.getTakeCount()+userIds.size());
    	return Response.ok().entity(new ResultDataVo<>(200, "发送优惠劵成功")).build();
    }

}
