package com.weir.quarkus.shop.marketing.resource.admin;


import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.marketing.entity.Banner;
import com.weir.quarkus.shop.marketing.vo.BannerQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Tag(name = "管理后台 - Banner 管理")
@Path("/promotion/banner")
public class BannerResource {
	@Inject
	EntityManager em;
    @POST
    @Path("/create")
    @Operation(summary = "创建 Banner")
//    @PreAuthorize("@ss.hasPermission('promotion:banner:create')")
    @Transactional
    public Response createBanner(@Valid @RequestBody Banner banner) {
    	banner.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "创建广告成功" , banner)).build();
    }

    @PUT
    @Path("/update")
    @Operation(summary = "更新 Banner")
//    @PreAuthorize("@ss.hasPermission('promotion:banner:update')")
    @Transactional
    public Response updateBanner(@Valid @RequestBody Banner banner) {
    	Banner old = Banner.findById(banner.getId());
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "广告不存在")).build();
		}
    	banner.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "更新广告成功" , banner)).build();
    }

    @DELETE
    @Path("/delete")
    @Operation(summary = "删除 Banner")
    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:banner:delete')")
    public Response deleteBanner(@QueryParam("id") Integer id) {
    	Banner old = Banner.findById(id);
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "广告不存在")).build();
		}
    	Banner.deleteById(id);
    	return Response.ok().entity(new ResultDataVo<>(200, "删除广告成功" , id)).build();
    }

    @GET
    @Path("/get")
    @Operation(summary = "获得 Banner")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('promotion:banner:query')")
    public Response getBanner(@QueryParam("id") Integer id) {
    	Banner banner = Banner.findById(id);
    	return Response.ok().entity(new ResultDataVo<>(200, null, banner)).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得 Banner 分页")
//    @PreAuthorize("@ss.hasPermission('promotion:banner:query')")
    public Response getBannerPage(@Valid BannerQuery query) {
    	VuePageListVo<Banner> queryPage = QueryHelper.createQueryPage(em, Banner.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPage)).build();
    }

}
