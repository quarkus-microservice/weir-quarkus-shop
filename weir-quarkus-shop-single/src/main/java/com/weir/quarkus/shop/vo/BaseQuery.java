package com.weir.quarkus.shop.vo;

import lombok.Data;

import java.util.Date;

import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.QueryParam;

@Data
public class BaseQuery {

	@QueryParam(value = "page")
	@DefaultValue(value = "1")
	private Integer page;
	
	@QueryParam(value = "pageSize")
	@DefaultValue(value = "10")
	private Integer pageSize;

//	@QueryParam(value = "createTime")
	private Date createTime;

//	@QueryParam(value = "creator")
	private Integer creator;

//	@QueryParam(value = "updateTime")
	private Date updateTime;

//	@QueryParam(value = "updater")
	private Integer updater;

	@QueryParam(value = "remark")
	private String remark;
}
