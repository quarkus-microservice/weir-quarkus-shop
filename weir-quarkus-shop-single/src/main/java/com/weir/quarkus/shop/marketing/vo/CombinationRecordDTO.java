/** 
 * @Title: CombinationRecordDTO.java 
 * @Package com.weir.quarkus.shop.marketing.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年2月15日 15:18:04 
 * @version V1.0 
 */
package com.weir.quarkus.shop.marketing.vo;

import lombok.Data;

/** 
 * @Title: CombinationRecordDTO.java 
 * @Package com.weir.quarkus.shop.marketing.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年2月15日 15:18:04 
 * @version V1.0 
 */
@Data
public class CombinationRecordDTO {

	private Integer activityId;
	private Integer recordCount;
}
