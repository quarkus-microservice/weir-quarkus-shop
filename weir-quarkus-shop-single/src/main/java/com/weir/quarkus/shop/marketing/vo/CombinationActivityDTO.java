package com.weir.quarkus.shop.marketing.vo;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * 拼团活动 DO
 *
 * @author HUIHUI
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CombinationActivityDTO {

    /**
     * 活动编号
     */
    private Integer id;
    /**
     * 拼团名称
     */
    private String name;
    /**
     * 商品 SPU 编号
     *
     * 关联 ProductSpuDO 的 id
     */
    private Integer spuId;
    /**
     * 总限购数量
     */
    private Integer totalLimitCount;
    /**
     * 单次限购数量
     */
    private Integer singleLimitCount;
    /**
     * 开始时间
     */
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;
    /**
     * 几人团
     */
    private Integer userSize;
    /**
     * 虚拟成团
     */
    private Boolean virtualGroup;
    /**
     * 活动状态
     *
     * 枚举 {@link CommonStatusEnum}
     */
    private Integer status;
    /**
     * 限制时长（小时）
     */
    private Integer limitDuration;

    private List<CombinationProductVO> products;
    
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    // ========== 商品字段 ==========

    @Schema(description = "商品名称", // 从 SPU 的 name 读取
            example = "618大促")
    private String spuName;
    @Schema(description = "商品主图", // 从 SPU 的 picUrl 读取
            example = "https://www.iocoder.cn/xx.png")
    private String picUrl;
    @Schema(description = "商品市场价，单位：分", // 从 SPU 的 marketPrice 读取
            example = "50")
    private Integer marketPrice;

    // ========== 统计字段 ==========

    @Schema(description = "开团组数", example = "33")
    private Integer groupCount;

    @Schema(description = "成团组数", example = "20")
    private Integer groupSuccessCount;

    @Schema(description = "购买次数", example = "100")
    private Integer recordCount;
}
