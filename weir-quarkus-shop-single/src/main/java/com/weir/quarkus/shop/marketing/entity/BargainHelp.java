package com.weir.quarkus.shop.marketing.entity;

import com.weir.quarkus.shop.entity.BaseEntity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.*;

/**
 * 砍价助力 DO
 *
 * @author HUIHUI
 */
@Entity
@Table(name = "promotion_bargain_help")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BargainHelp extends BaseEntity {

    /**
     * 编号
     */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 砍价活动编号
     *
     * 关联 {@link BargainActivity#getId()} 字段
     */
    private Integer activityId;
    /**
     * 砍价记录编号
     *
     * 关联 {@link BargainRecord#getId()} 字段
     */
    private Integer recordId;

    /**
     * 用户编号
     */
    private Integer userId;
    /**
     * 减少价格，单位：分
     */
    private Integer reducePrice;

}
