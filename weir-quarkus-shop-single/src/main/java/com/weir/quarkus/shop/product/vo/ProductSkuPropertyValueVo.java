package com.weir.quarkus.shop.product.vo;

import lombok.*;

/**
 * 商品 SKU-属性关联表
 * @Title: ProductSkuProperty.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品 SKU-属性关联表
 * @author weir
 * @date 2023年12月13日 15:44:22 
 * @version V1.0
 */
@Data
public class ProductSkuPropertyValueVo {

    private Integer id;
    /**
     * SPU ID
     *
     */
    private Integer skuId;
    /**
     * 属性ID
     */
    private Integer propertyId;
    private String propertyName;
	/**
     * 属性值ID
     */
	private Integer propertyValueId;
	private String propertyValueName;
    
    private Integer spuId;
}

