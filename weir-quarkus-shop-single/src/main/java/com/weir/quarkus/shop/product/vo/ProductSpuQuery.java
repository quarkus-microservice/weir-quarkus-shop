package com.weir.quarkus.shop.product.vo;

import com.weir.quarkus.shop.vo.BaseQuery;

import lombok.*;

/**
 * 商品 SPU DO
 *
 * @author 芋道源码
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductSpuQuery extends BaseQuery {

    private Integer id;

    /**
     * 商品名称
     */
    private String name;
    /**
     * 关键字
     */
    private String keyword;
    /**
     * 商品简介
     */
    private String introduction;
    /**
     * 商品详情
     */
    private String description;
    /**
     * 商品条码（一维码）
     */
    private String barCode;

    /**
     * 商品分类编号
     *
     */
    private Integer categoryId;
    /**
     * 商品品牌编号
     *
     */
    private Integer brandId;
    /**
     * 商品封面图
     */
    private String picUrl;
    /**
     * 商品轮播图(逗号分隔)
     */
    private String sliderPicUrls;
    /**
     * 商品视频
     */
    private String videoUrl;

    /**
     * 单位
     *
     */
    private Integer unit;
    /**
     * 排序字段
     */
    private Integer sort;
    /**
     * 商品状态
     *
     */
    private Integer status;

    /**
     * 规格类型
     *
     * false - 单规格
     * true - 多规格
     */
    private Boolean specType;
    /**
     * 商品价格，单位使用：分
     *
     */
    private Integer price;
    /**
     * 市场价，单位使用：分
     *
     */
    private Integer marketPrice;
    /**
     * 成本价，单位使用：分
     *
     */
    private Integer costPrice;
    /**
     * 库存
     *
     */
    private Integer stock;

    /**
     * 物流配置模板编号
     *
     */
    private Integer deliveryTemplateId;

    /**
     * 是否热卖推荐
     */
    private Boolean recommendHot;
    /**
     * 是否优惠推荐
     */
    private Boolean recommendBenefit;
    /**
     * 是否精品推荐
     */
    private Boolean recommendBest;
    /**
     * 是否新品推荐
     */
    private Boolean recommendNew;
    /**
     * 是否优品推荐
     */
    private Boolean recommendGood;

    /**
     * 赠送积分
     */
    private Integer giveIntegral;
    /**
     * 赠送的优惠劵编号的数组(逗号分隔)
     *
     */
    private String giveCouponTemplateIds;

    /**
     * 分销类型
     *
     * false - 默认
     * true - 自行设置
     */
    private Boolean subCommissionType;

    /**
     * 活动展示顺序(逗号分隔)
     *
     */
    private String activityOrders;

    /**
     * 商品销量
     */
    private Integer salesCount;
    /**
     * 虚拟销量
     */
    private Integer virtualSalesCount;
    /**
     * 浏览量
     */
    private Integer browseCount;
}
