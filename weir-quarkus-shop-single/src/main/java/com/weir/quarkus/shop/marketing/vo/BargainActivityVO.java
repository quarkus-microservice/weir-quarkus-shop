package com.weir.quarkus.shop.marketing.vo;

import lombok.*;

import java.time.LocalDateTime;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * 砍价活动 DO
 *
 * @author HUIHUI
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BargainActivityVO {

    /**
     * 砍价活动编号
     */
    private Integer id;

    /**
     * 砍价活动名称
     */
    private String name;

    /**
     * 活动开始时间
     */
    private LocalDateTime startTime;
    /**
     * 活动结束时间
     */
    private LocalDateTime endTime;

    /**
     * 活动状态
     *
     * 枚举 {@link CommonStatusEnum}
     */
    private Integer status = 0;

    /**
     * 商品 SPU 编号
     */
    private Integer spuId;
    /**
     * 商品 SKU 编号
     */
    private Integer skuId;
    /**
     * 砍价起始价格，单位：分
     */
    private Integer bargainFirstPrice;
    /**
     * 砍价底价，单位：分
     */
    private Integer bargainMinPrice;

    /**
     * 砍价库存(剩余库存砍价时扣减)
     */
    private Integer stock;
    /**
     * 砍价总库存
     */
    private Integer totalStock;

    /**
     * 砍价人数
     *
     * 需要多少人，砍价才能成功，即 {@link BargainRecordQuery#getStatus()} 更新为 {@link BargainRecordQuery#getStatus()} 成功状态
     */
    private Integer helpMaxCount;
    /**
     * 帮砍次数
     *
     * 单个活动，用户可以帮砍的次数。
     * 例如说：帮砍次数为 1 时，A 和 B 同时将该活动链接发给 C，C 只能帮其中一个人砍价。
     */
    private Integer bargainCount;

    /**
     * 总限购数量
     */
    private Integer totalLimitCount;
    /**
     * 用户每次砍价的最小金额，单位：分
     */
    private Integer randomMinPrice;
    /**
     * 用户每次砍价的最大金额，单位：分
     */
    private Integer randomMaxPrice;
    
    @Schema(description = "商品名称", example = "618大促")
    private String spuName;
    @Schema(description = "商品主图", example = "https://www.iocoder.cn/xx.png")
    private String picUrl;
    
    @Schema(description = "总砍价的用户数量", example = "999")
    private Integer recordUserCount;

    @Schema(description = "成功砍价的用户数量", example = "500")
    private Integer recordSuccessUserCount;

    @Schema(description = "帮助砍价的用户数量", example = "888")
    private Integer helpUserCount;

}
