package com.weir.quarkus.shop.marketing.vo;

import java.time.LocalDateTime;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.weir.quarkus.shop.marketing.entity.CombinationActivity;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CombinationProductVO {

    @Schema(description = "商品 spuId",  example = "30563")
    @NotNull(message = "商品 spuId 不能为空")
    private Integer spuId;

    @Schema(description = "商品 skuId",  example = "30563")
    @NotNull(message = "商品 skuId 不能为空")
    private Integer skuId;

    @Schema(description = "拼团价格，单位分", example = "27682")
    @NotNull(message = "拼团价格不能为空")
    private Integer combinationPrice;

    private Integer id;
    /**
     * 拼团活动编号
     */
    private Integer activityId;
    
    /**
     * 拼团商品状态
     *
     * 关联 {@link CombinationActivity#getStatus()}
     */
    private Integer activityStatus;
    /**
     * 活动开始时间点
     *
     * 冗余 {@link CombinationActivity#getStartTime()}
     */
    private LocalDateTime activityStartTime;
    /**
     * 活动结束时间点
     *
     * 冗余 {@link CombinationActivity#getEndTime()}
     */
    private LocalDateTime activityEndTime;

}