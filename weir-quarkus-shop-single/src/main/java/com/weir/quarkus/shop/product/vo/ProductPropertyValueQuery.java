package com.weir.quarkus.shop.product.vo;

import jakarta.ws.rs.QueryParam;

import com.weir.quarkus.shop.vo.BaseQuery;

import lombok.*;


/**
 * 
 * @Title: ProductPropertyValue.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品属性值
 * @author weir
 * @date 2023年12月12日 10:11:14 
 * @version V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductPropertyValueQuery extends BaseQuery {
	
    private Integer id;
    /**
     * 属性项的编号
     *
     */
    @QueryParam(value = "propertyId")
    private Integer propertyId;
    /**
     * 名称
     */
    @QueryParam(value = "name")
    private String name;
    /**
     * 备注
     *
     */
    private String remark;

}
