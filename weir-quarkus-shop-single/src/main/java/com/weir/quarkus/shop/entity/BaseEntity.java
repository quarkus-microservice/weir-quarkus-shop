package com.weir.quarkus.shop.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

import com.fasterxml.jackson.annotation.JsonFormat;
//import com.weir.quarkus.base.system.handler.BaseEntityListener;
import com.weir.quarkus.shop.product.entity.ProductBrand;

import jakarta.persistence.Column;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Transient;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 
 * @ClassName: BaseEntity
 * @Description: 实体公共基类（表的创建时间 创建人 修改时间 修改人 分页的当前页 每页数据）
 * @author weir
 * @date 2021年8月25日
 *
 */
@Data
@EqualsAndHashCode(callSuper = true)
@MappedSuperclass
//@EntityListeners(BaseEntityListener.class)
public class BaseEntity extends PanacheEntityBase {

//	@Transient
//	private Integer page = 0;
//	@Transient
//	private Integer rows = 10;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "create_time")
	private Date createTime;

	private Integer creator;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	@Column(name = "update_time")
	private Date updateTime;

	private Integer updater;

	private String remark;

}