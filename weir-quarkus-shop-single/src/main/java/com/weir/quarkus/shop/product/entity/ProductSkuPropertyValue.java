package com.weir.quarkus.shop.product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.NamedNativeQuery;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

import com.weir.quarkus.shop.entity.BaseEntity;
import com.weir.quarkus.shop.product.vo.ProductSkuPropertyValueVo;

import lombok.*;

/**
 * 商品 SKU-属性关联表
 * @Title: ProductSkuProperty.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品 SKU-属性关联表
 * @author weir
 * @date 2023年12月13日 15:44:22 
 * @version V1.0
 */
@Entity
@Table(name = "product_sku_property_value")
@Data
@EqualsAndHashCode(callSuper = true)
@NamedNativeQuery(name = "ProductSkuPropertyValue.listBySpuId",query = "SELECT pspv.id,pspv.spu_id,pspv.sku_id,pspv.property_id,pspv.property_value_id,\n"
		+ "pp.name as property_name,ppv.name as property_value_name\n"
		+ "from product_sku_property_value pspv\n"
		+ "join product_property pp on pspv.property_id=pp.id\n"
		+ "join product_property_value ppv on pspv.property_value_id=ppv.id where pspv.spu_id = ?",resultClass = ProductSkuPropertyValueVo.class)
public class ProductSkuPropertyValue extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * SPU ID
     *
     */
	@Column(name = "sku_id")
    private Integer skuId;
    /**
     * 属性ID
     */
	@Column(name = "property_id")
    private Integer propertyId;
	/**
     * 属性值ID
     */
	@Column(name = "property_value_id")
	private Integer propertyValueId;
    
	@Column(name = "spu_id")
    private Integer spuId;
	
	public static void listBySpuId(Integer spuId) {
		
	}
}

