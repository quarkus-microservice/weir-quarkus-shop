package com.weir.quarkus.shop.marketing.vo;

import com.weir.quarkus.shop.vo.BaseQuery;

import jakarta.validation.constraints.NotNull;
import lombok.*;

/**
 * 文章管理 DO
 *
 * @author HUIHUI
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ArticleQuery extends BaseQuery {

    /**
     * 文章管理编号
     */
    private Integer id;
    /**
     * 分类编号 ArticleCategoryDO#id
     */
	@NotNull(message = "关联商品不能为空")
    private Integer categoryId;
    /**
     * 关联商品编号 ProductSpuDO#id
     */
    private Integer spuId;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 文章作者
     */
    private String author;
    /**
     * 文章封面图片地址
     */
    private String picUrl;
    /**
     * 文章简介
     */
    private String introduction;
    /**
     * 浏览次数
     */
    private Integer browseCount = 0;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 状态
     *
     * 枚举 {@link CommonStatusEnum}
     */
    private Integer status;
    /**
     * 是否热门(小程序)
     */
    private Boolean recommendHot;
    /**
     * 是否轮播图(小程序)
     */
    private Boolean recommendBanner;
    /**
     * 文章内容
     */
    private String content;

}
