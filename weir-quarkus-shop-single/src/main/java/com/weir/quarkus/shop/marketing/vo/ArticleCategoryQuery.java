package com.weir.quarkus.shop.marketing.vo;

import com.weir.quarkus.shop.vo.BaseQuery;

import lombok.*;

/**
 * 文章分类 DO
 *
 * @author HUIHUI
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ArticleCategoryQuery extends BaseQuery {

    /**
     * 文章分类编号
     */
    private Integer id;
    /**
     * 文章分类名称
     */
    private String name;
    /**
     * 图标地址
     */
    private String picUrl;
    /**
     * 状态
     *
     * 枚举 {@link CommonStatusEnum}
     */
    private Integer status;
    /**
     * 排序
     */
    private Integer sort;

}
