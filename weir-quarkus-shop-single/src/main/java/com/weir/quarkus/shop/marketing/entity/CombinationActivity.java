package com.weir.quarkus.shop.marketing.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.*;

import java.time.LocalDateTime;

import com.weir.quarkus.shop.entity.BaseEntity;

/**
 * 拼团活动 DO
 *
 * @author HUIHUI
 */
@Entity
@Table(name = "promotion_combination_activity")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CombinationActivity extends BaseEntity {

    /**
     * 活动编号
     */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 拼团名称
     */
    private String name;
    /**
     * 商品 SPU 编号
     *
     * 关联 ProductSpuDO 的 id
     */
    private Integer spuId;
    /**
     * 总限购数量
     */
    private Integer totalLimitCount;
    /**
     * 单次限购数量
     */
    private Integer singleLimitCount;
    /**
     * 开始时间
     */
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;
    /**
     * 几人团
     */
    private Integer userSize;
    /**
     * 虚拟成团
     */
    private Boolean virtualGroup;
    /**
     * 活动状态
     *
     * 枚举 {@link CommonStatusEnum}
     */
    private Integer status;
    /**
     * 限制时长（小时）
     */
    private Integer limitDuration;

}
