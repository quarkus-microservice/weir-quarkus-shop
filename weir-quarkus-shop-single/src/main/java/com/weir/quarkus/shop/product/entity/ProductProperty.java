package com.weir.quarkus.shop.product.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import java.util.List;

import com.weir.quarkus.shop.entity.BaseEntity;

import lombok.*;

/**
 * 
 * @Title: ProductProperty.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品属性项
 * @author weir
 * @date 2023年12月12日 10:11:27 
 * @version V1.0
 */
@Entity
@Table(name = "product_property")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductProperty extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 名称
     */
    private String name;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;

    @Transient
    private List<ProductPropertyValue> values;
}
