package com.weir.quarkus.shop.product.resource.admin;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.service.ProductFavoriteService;
import com.weir.quarkus.shop.product.vo.ProductFavoriteQuery;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 商品收藏")
@Path("/product/favorite")
public class ProductFavoriteResource {

	@Inject
	ProductFavoriteService productFavoriteService;

	@GET
	@Path("/page")
	@Operation(summary = "获得商品收藏分页")
//    @PreAuthorize("@ss.hasPermission('product:favorite:query')")
	public Response getFavoritePage(ProductFavoriteQuery query) {
		VuePageListVo vuePageListVo = productFavoriteService.queryPage(query);
		return Response.ok().entity(new ResultDataVo<>(200, vuePageListVo)).build();
	}

}
