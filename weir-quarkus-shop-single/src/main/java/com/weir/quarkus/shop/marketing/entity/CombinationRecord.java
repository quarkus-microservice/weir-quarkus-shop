package com.weir.quarkus.shop.marketing.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import com.weir.quarkus.shop.entity.BaseEntity;

import lombok.*;

import java.time.LocalDateTime;

/**
 * 拼团记录 DO
 *
 * 1. 用户参与拼团时，会创建一条记录
 * 2. 团长的拼团记录，和参团人的拼团记录，通过 {@link #headId} 关联
 *
 * @author HUIHUI
 */
@Entity
@Table(name = "promotion_combination_record")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CombinationRecord extends BaseEntity {

    /**
     * 编号，主键自增
     */
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 拼团活动编号
     *
     * 关联 {@link CombinationActivity#getId()}
     */
    private Integer activityId;
    /**
     * 拼团商品单价
     *
     * 冗余 {@link CombinationProduct#getCombinationPrice()}
     */
    private Integer combinationPrice;
    /**
     * SPU 编号
     */
    private Integer spuId;
    /**
     * 商品名字
     */
    private String spuName;
    /**
     * 商品图片
     */
    private String picUrl;
    /**
     * SKU 编号
     */
    private Integer skuId;
    /**
     * 购买的商品数量
     */
    private Integer count;

    /**
     * 用户编号
     */
    private Integer userId;

    /**
     * 用户昵称
     */
    private String nickname;
    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 团长编号
     *
     * 关联 {@link CombinationRecord#getId()}
     *
     * 如果是团长，则它的值是 {@link #HEAD_ID_GROUP}
     */
    private Integer headId;
    /**
     * 开团状态
     *
     * 关联 {@link CombinationRecordStatusEnum}
     */
    private Integer status;
    /**
     * 订单编号
     */
    private Integer orderId;
    /**
     * 开团需要人数
     *
     * 关联 {@link CombinationActivity#getUserSize()}
     */
    private Integer userSize;
    /**
     * 已加入拼团人数
     */
    private Integer userCount;
    /**
     * 是否虚拟成团
     *
     * 默认为 false。
     * 拼团过期都还没有成功，如果 {@link CombinationActivity#getVirtualGroup()} 为 true，则执行虚拟成团的逻辑，才会更新该字段为 true
     */
    private Boolean virtualGroup;

    /**
     * 过期时间
     *
     * 基于 {@link CombinationRecord#getStartTime()} + {@link CombinationActivity#getLimitDuration()} 计算
     */
    private LocalDateTime expireTime;
    /**
     * 开始时间 (订单付款后开始的时间)
     */
    private LocalDateTime startTime;
    /**
     * 结束时间（成团时间/失败时间）
     */
    private LocalDateTime endTime;

}
