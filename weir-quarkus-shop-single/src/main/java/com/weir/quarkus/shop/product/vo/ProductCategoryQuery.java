package com.weir.quarkus.shop.product.vo;

import com.weir.quarkus.shop.vo.BaseQuery;

import jakarta.validation.constraints.NotBlank;
import jakarta.ws.rs.QueryParam;
import lombok.*;

/**
 * 
 * @Title: ProductCategoryDO.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品分类
 * @author weir
 * @date 2023年12月12日 09:56:32 
 * @version V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductCategoryQuery extends BaseQuery {
    private Integer id;
    /**
     * 父分类编号
     */
    @QueryParam(value = "parentId")
    private Integer parentId;
    /**
     * 分类名称
     */
    @QueryParam(value = "name")
    private String name;
    /**
     * 移动端分类图
     *
     * 建议 180*180 分辨率
     */
    private String picUrl;
    /**
     * PC 端分类图
     *
     * 建议 468*340 分辨率
     */
    private String bigPicUrl;
    /**
     * 分类排序
     */
    private Integer sort;
    /**
     * 开启状态
     *
     */
    private Integer status;

}
