package com.weir.quarkus.shop.product.resource.admin;

import java.util.Date;

import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductComment;
import com.weir.quarkus.shop.product.entity.ProductSku;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.vo.ProductCommentQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

import io.quarkus.runtime.util.StringUtil;
import io.vertx.core.json.JsonObject;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Tag(name = "管理后台 - 商品评价")
@Path("/product/comment")
public class ProductCommentResource {

	@Inject
	EntityManager em;
	
	@Inject
    JsonWebToken jwt;
	
//	public SysUser getUser() {
//		Object claim = jwt.getClaim(Claims.preferred_username.name());
//		return new JsonObject(new JsonObject(claim.toString()).getValue("user").toString()).mapTo(SysUser.class);
//	}
	
	@GET
    @Path("/page")
    @Operation(summary = "获得商品评价分页")
//    @PreAuthorize("@ss.hasPermission('product:comment:query')")
    public Response getCommentPage(@BeanParam ProductCommentQuery query) {
		VuePageListVo<ProductComment> queryPage = QueryHelper.createQueryPage(em, ProductComment.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, queryPage)).build();
    }

	@PUT
    @Path("/update-visible")
    @Operation(summary = "显示 / 隐藏评论")
//    @PreAuthorize("@ss.hasPermission('product:comment:update')")
	@Transactional
    public Response updateCommentVisible(ProductComment comment) {
        if (comment.getId() == null || comment.getVisible() == null) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "评论ID或是否可见值不能为空")).build();
		}
        ProductComment pc = ProductComment.findById(comment.getId());
        if (pc == null) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "评论不存在")).build();
		}
        pc.setVisible(comment.getVisible());
        return Response.ok().entity(new ResultDataVo<>(200, pc)).build();
    }

//	@PUT
//    @Path("/reply")
//    @Operation(summary = "商家回复")
////    @PreAuthorize("@ss.hasPermission('product:comment:update')")
//	@Transactional
//    public Response commentReply(ProductComment comment) {
//		SysUser user = null;
//		if (user == null) {
//			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "登录信息未获取到，请重新登录")).build();
//		}
//		if (comment.getId() == null || StringUtil.isNullOrEmpty(comment.getReplyContent())) {
//        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "评论ID或回复内容不能为空")).build();
//		}
//        ProductComment pc = ProductComment.findById(comment.getId());
//        if (pc == null) {
//        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "评论不存在")).build();
//		}
//        pc.setReplyContent(comment.getReplyContent());
//        pc.setReplyTime(new Date());
//        pc.setReplyStatus(true);
//        pc.setReplyUserId(user.getId());
//        return Response.ok().entity(new ResultDataVo<>(200, pc)).build();
//    }

	@POST
    @Path("/create")
    @Operation(summary = "添加自评")
//    @PreAuthorize("@ss.hasPermission('product:comment:update')")
	@Transactional
    public Response createComment(ProductComment comment) {
		if (comment.getSkuId() == null || comment.getSpuId() == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品SKU或SPU ID不能为空")).build();
		}
		ProductSku sku = ProductSku.findById(comment.getSkuId());
		ProductSpu spu = ProductSpu.findById(comment.getSpuId());
		if (sku == null || spu == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品SKU或SPU信息不存在")).build();
		}
		comment.setSkuPicUrl(sku.getPicUrl());
//		comment.setSkuProperties(sku.getProperties());
		comment.setSpuName(spu.getName());
		comment.persist();
		return Response.ok().entity(new ResultDataVo<>(200, comment)).build();
    }

}
