package com.weir.quarkus.shop.product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import com.weir.quarkus.shop.entity.BaseEntity;

import lombok.*;


/**
 * 
 * @Title: ProductPropertyValue.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品属性值
 * @author weir
 * @date 2023年12月12日 10:11:14 
 * @version V1.0
 */
@Entity
@Table(name = "product_property_value")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductPropertyValue extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 属性项的编号
     *
     */
	@Column(name = "property_id")
    private Integer propertyId;
    /**
     * 名称
     */
    private String name;
    /**
     * 备注
     *
     */
    private String remark;

    public static boolean checkCountPropertyId(Integer propertyId) {
		long count = count("propertyId", propertyId);
		if (count > 0) {
			return true;
		}
		return false;
	}
}
