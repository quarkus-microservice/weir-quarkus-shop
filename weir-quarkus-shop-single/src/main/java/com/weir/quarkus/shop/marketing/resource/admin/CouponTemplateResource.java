package com.weir.quarkus.shop.marketing.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.marketing.entity.CouponTemplate;
import com.weir.quarkus.shop.marketing.enums.PromotionProductScopeEnum;
import com.weir.quarkus.shop.marketing.vo.CouponTemplateQuery;
import com.weir.quarkus.shop.product.entity.ProductCategory;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 优惠劵模板")
@Path("/promotion/coupon-template")
public class CouponTemplateResource {

	@Inject EntityManager em;
	
    @POST
    @Path("/create")
    @Operation(summary = "创建优惠劵模板")
//    @PreAuthorize("@ss.hasPermission('promotion:coupon-template:create')")
    public Response createCouponTemplate(CouponTemplate couponTemplate) {
    	Response checkProductScope = checkProductScope(couponTemplate);
    	if (checkProductScope != null) {
			return checkProductScope;
		}
        couponTemplate.persist();
        return Response.ok().entity(new ResultDataVo<>(200, "创建优惠劵模板成功", couponTemplate)).build();
    }

	private Response checkProductScope(CouponTemplate couponTemplate) {
		Integer productScope = couponTemplate.getProductScope();
        String productScopeValues = couponTemplate.getProductScopeValues();
        List<String> ids = Arrays.asList(StringUtils.split(productScopeValues,",")); 
        if (PromotionProductScopeEnum.SPU.getScope().equals(productScope)) {
			List<ProductSpu> spus = ProductSpu.list("id in (?1)", ids);
			Map<Integer, ProductSpu> spuMap = spus.stream().collect(Collectors.toMap(ProductSpu::getId, a -> a, (k1, k2) -> k1));
			for (String id : ids) {
				ProductSpu productSpu = spuMap.get(Integer.valueOf(id));
				if (productSpu == null) {
					return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SPU 不存在")).build();
				}
				if (productSpu.getStatus() != 0) {
					return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SPU 不处于上架状态")).build();
				}
			}
		}else if (PromotionProductScopeEnum.CATEGORY.getScope().equals(productScope)) {
			List<ProductCategory> pcs = ProductCategory.list("id in (?1)", ids);
			Map<Integer, ProductCategory> pcMap = pcs.stream().collect(Collectors.toMap(ProductCategory::getId, a -> a, (k1, k2) -> k1));
			for (String id : ids) {
				ProductCategory productCategory = pcMap.get(Integer.valueOf(id));
				if (productCategory == null) {
					return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品分类不存在")).build();
				}
				if (productCategory.getStatus() != 0) {
					return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品分类已禁用，无法使用")).build();
				}
			}
		}
        return null;
	}

    @PUT
    @Path("/update")
    @Operation(summary = "更新优惠劵模板")
//    @PreAuthorize("@ss.hasPermission('promotion:coupon-template:update')")
    public Response updateCouponTemplate(CouponTemplate couponTemplate) {
    	CouponTemplate old = CouponTemplate.findById(couponTemplate.getId());
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "优惠劵模板不存在")).build();
		}
    	if (old.getTotalCount() < old.getTakeCount()) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "发放数量不能小于已领取数量")).build();
		}
    	Response checkProductScope = checkProductScope(couponTemplate);
    	if (checkProductScope != null) {
			return checkProductScope;
		}
    	couponTemplate.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "更新优惠劵模板成功", couponTemplate)).build();
    }

    @PUT
    @Path("/update-status")
    @Operation(summary = "更新优惠劵模板状态")
//    @PreAuthorize("@ss.hasPermission('promotion:coupon-template:update')")
    public Response updateCouponTemplateStatus(CouponTemplate couponTemplate) {
    	CouponTemplate old = CouponTemplate.findById(couponTemplate.getId());
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "优惠劵模板不存在")).build();
		}
    	old.setStatus(couponTemplate.getStatus());
    	return Response.ok().entity(new ResultDataVo<>(200, "更新优惠劵模板状态成功", couponTemplate)).build();
    }

    @DELETE
    @Path("/delete")
    @Operation(summary = "删除优惠劵模板")
    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:coupon-template:delete')")
    public Response deleteCouponTemplate(@QueryParam("id") Integer id) {
    	CouponTemplate old = CouponTemplate.findById(id);
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "优惠劵模板不存在")).build();
		}
    	old.delete();
    	return Response.ok().entity(new ResultDataVo<>(200, "删除优惠劵模板成功", id)).build();
    }

    @GET
    @Path("/get")
    @Operation(summary = "获得优惠劵模板")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('promotion:coupon-template:query')")
    public Response getCouponTemplate(@QueryParam("id") Integer id) {
    	return Response.ok().entity(new ResultDataVo<>(200, CouponTemplate.findById(id))).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得优惠劵模板分页")
//    @PreAuthorize("@ss.hasPermission('promotion:coupon-template:query')")
    public Response getCouponTemplatePage(@Valid CouponTemplateQuery query) {
    	VuePageListVo<CouponTemplate> queryPage = QueryHelper.createQueryPage(em, CouponTemplate.class,
				query);
    	return Response.ok().entity(new ResultDataVo<>(200, queryPage)).build();
    }

    @GET
    @Path("/list")
    @Operation(summary = "获得优惠劵模板列表")
    @Parameter(name = "ids", description = "编号列表", required = true, example = "1024,2048")
//    @PreAuthorize("@ss.hasPermission('promotion:coupon-template:query')")
    public Response getCouponTemplateList(@QueryParam("ids") String ids) {
    	return Response.ok().entity(new ResultDataVo<>(200, CouponTemplate.list("id in (?1)",ids.split(",")))).build();
    }

}
