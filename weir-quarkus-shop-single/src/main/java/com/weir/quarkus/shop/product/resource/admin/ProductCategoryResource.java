package com.weir.quarkus.shop.product.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.Comparator;
import java.util.List;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductCategory;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.vo.ProductCategoryQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;

@Tag(name = "管理后台 - 商品分类")
@Path("/product/category")
public class ProductCategoryResource {
	@Inject
	EntityManager em;
	
	@POST
    @Path("/create")
    @Operation(summary = "创建商品分类")
//    @PreAuthorize("@ss.hasPermission('product:category:create')")
	@Transactional
    public Response createCategory(ProductCategory category) {
		if (ProductCategory.checkParentIdExist(category.getParentId())) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "父分类不存在")).build();
		}
        category.persist();
        return Response.ok().entity(new ResultDataVo<>(200, "创建商品分类成功" , category)).build();
    }

	@PUT
    @Path("/update")
    @Operation(summary = "更新商品分类")
//    @PreAuthorize("@ss.hasPermission('product:category:update')")
	@Transactional
    public Response updateCategory(ProductCategory category) {
        if (category.getId() == null) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "id不能为空")).build();
		}
        ProductCategory c = ProductCategory.findById(category.getId());
        if (c == null) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该商品分类不存在")).build();
		}
        if (ProductCategory.checkParentIdExist(category.getParentId())) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "父分类不存在")).build();
		}
        c.setBigPicUrl(category.getBigPicUrl());
        c.setName(category.getName());
        c.setParentId(category.getParentId());
        c.setPicUrl(category.getPicUrl());
        c.setRemark(category.getRemark());
        c.setSort(category.getSort());
        c.setStatus(category.getStatus());
        return Response.ok().entity(new ResultDataVo<>(200, "更新商品分类成功" , c)).build();
    }

	@DELETE
    @Path("/delete")
    @Operation(summary = "删除商品分类")
//    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('product:category:delete')")
	@Transactional
    public Response deleteCategory(@QueryParam("id") Integer id) {
		ProductCategory c = ProductCategory.findById(id);
		if (c == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该商品分类不存在")).build();
		}
        if (ProductCategory.countParentIdExist(id)) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "还存在子商品分类，不能删除该分类")).build();
		}
        // 校验分类是否绑定了 SPU
        if(ProductSpu.checkCountByCategoryId(id)) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "类别下存在商品，不能删除")).build();
        }
        c.delete();
        return Response.ok().entity(new ResultDataVo<>(200, "删除商品分类成功")).build();
    }

	@GET
    @Path("/get")
    @Operation(summary = "获得商品分类")
//    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('product:category:query')")
    public Response getCategory(@QueryParam("id") Integer id) {
		return Response.ok().entity(new ResultDataVo<>(200, ProductCategory.findById(id))).build();
    }

	@GET
    @Path("/list")
    @Operation(summary = "获得商品分类列表")
//    @PreAuthorize("@ss.hasPermission('product:category:query')")
    public Response getCategoryList(@BeanParam ProductCategoryQuery query) {
		List<ProductCategory> queryReturn = QueryHelper.createQueryReturn(em, ProductCategory.class, query).getResultList();
		queryReturn.sort(Comparator.comparing(ProductCategory::getSort));
		return Response.ok().entity(new ResultDataVo<>(200, queryReturn)).build();
    }

}
