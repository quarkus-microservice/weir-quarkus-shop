package com.weir.quarkus.shop.marketing.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.shop.marketing.entity.BargainActivity;
import com.weir.quarkus.shop.marketing.entity.BargainRecord;
import com.weir.quarkus.shop.marketing.entity.QBargainHelp;
import com.weir.quarkus.shop.marketing.mapper.BargainActivityMapper;
import com.weir.quarkus.shop.marketing.mapper.BargainRecordMapper;
import com.weir.quarkus.shop.marketing.vo.BargainActivityDTO;
import com.weir.quarkus.shop.marketing.vo.BargainActivityVO;
import com.weir.quarkus.shop.marketing.vo.BargainRecordQuery;
import com.weir.quarkus.shop.marketing.vo.BargainRecordVO;
import com.weir.quarkus.shop.member.entity.MemberUser;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 砍价记录")
@Path("/promotion/bargain-record")
public class BargainRecordResource {

	private static final QBargainHelp qBargainHelp = QBargainHelp.bargainHelp;

	@Inject EntityManager em;
	@Inject JPAQueryFactory jpaQueryFactory;
	@Inject BargainActivityMapper bargainActivityMapper;
	@Inject BargainRecordMapper bargainRecordMapper;

	@GET
	@Path("/page")
	@Operation(summary = "获得砍价记录分页")
//    @PreAuthorize("@ss.hasPermission('promotion:bargain-record:query')")
	public Response getBargainRecordPage(@Valid BargainRecordQuery query) {
		VuePageListVo<BargainRecord> queryPage = QueryHelper.createQueryPage(em, BargainRecord.class, query);
		List<BargainRecord> bargainRecords = queryPage.getList();
		List<Integer> userIds = bargainRecords.stream().map(BargainRecord::getUserId).distinct()
				.collect(Collectors.toList());
		List<MemberUser> memberUsers = MemberUser.list("id in (?1)", userIds);
		Map<Integer, MemberUser> memberUserMap = memberUsers.stream()
				.collect(Collectors.toMap(MemberUser::getId, a -> a, (k1, k2) -> k1));
		List<Integer> activityIds = bargainRecords.stream().map(BargainRecord::getActivityId).distinct()
				.collect(Collectors.toList());
		List<BargainActivity> bargainActivities = BargainActivity.list("id in (?1)", activityIds);
		List<BargainActivityVO> bargainActivityVOs = bargainActivityMapper.toVo(bargainActivities);

		List<Integer> ids = bargainRecords.stream().map(BargainRecord::getId).distinct().collect(Collectors.toList());
		List<BargainActivityDTO> helpList = jpaQueryFactory
				.select(Projections.fields(BargainActivityDTO.class, qBargainHelp.recordId,
						qBargainHelp.count().as("userCount")))
				.from(qBargainHelp).where(qBargainHelp.recordId.in(ids)).groupBy(qBargainHelp.recordId).fetch();
		Map<Integer, BargainActivityDTO> helpMap = helpList.stream().collect(Collectors.toMap(BargainActivityDTO::getRecordId, a -> a, (k1, k2) -> k1));
		bargainActivityVOs.forEach(b->{
			BargainActivityDTO bargainActivityDTO = helpMap.get(b.getId());
			if (bargainActivityDTO != null) {				
				b.setHelpUserCount(bargainActivityDTO.getRecordId());
			}
		});
		Map<Integer, BargainActivityVO> bargainActivityVOMap = bargainActivityVOs.stream()
				.collect(Collectors.toMap(BargainActivityVO::getId, a -> a, (k1, k2) -> k1));
		
		List<BargainRecordVO> bargainRecordVOs = bargainRecordMapper.toVo(bargainRecords);
		bargainRecordVOs.forEach(b->{
			MemberUser memberUser = memberUserMap.get(b.getUserId());
			if (memberUser != null) {
				b.setNickname(memberUser.getNickname());
				b.setAvatar(memberUser.getAvatar());
			}
			BargainActivityVO bargainActivityVO = bargainActivityVOMap.get(b.getId());
			if (bargainActivityVO != null) {
				b.setActivity(bargainActivityVO);
			}
		});
		VuePageListVo<BargainRecordVO> queryPageVo = new VuePageListVo<>(bargainRecordVOs,queryPage.getPage(),queryPage.getPageSize(),queryPage.getPageCount(),queryPage.getTotal());
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPageVo)).build();
	}

}
