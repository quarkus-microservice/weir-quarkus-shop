package com.weir.quarkus.shop.product.resource.app;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductComment;
import com.weir.quarkus.shop.product.vo.ProductCommentQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Tag(name = "用户 APP - 商品评价")
@Path("app/product/comment")
public class AppProductCommentResource {

	@Inject
	EntityManager em;
	
    @GET
    @Path("/page")
    @Operation(summary = "获得商品评价分页")
    public Response getCommentPage(@BeanParam ProductCommentQuery query) {
    	VuePageListVo<ProductComment> queryPage = QueryHelper.createQueryPage(em, ProductComment.class, query);
    	return Response.ok().entity(new ResultDataVo<>(200, queryPage)).build();
    }

}
