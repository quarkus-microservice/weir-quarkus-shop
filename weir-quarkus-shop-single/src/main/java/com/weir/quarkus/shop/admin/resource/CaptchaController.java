package com.weir.quarkus.shop.admin.resource;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Objects;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.weir.quarkus.base.system.utils.IpUtil;
import com.weir.quarkus.shop.captcha.vo.CaptchaVO;
import com.weir.quarkus.shop.captcha.vo.PointVO;
import com.weir.quarkus.shop.utils.AESUtil;
import com.weir.quarkus.shop.utils.CaptchaServiceFactory;
import com.weir.quarkus.shop.utils.ImageUtils;
import com.weir.quarkus.shop.utils.RandomUtils;
import com.weir.quarkus.shop.vo.ResultDataVo;

import io.vertx.core.http.HttpServerRequest;
import jakarta.annotation.Resource;
import jakarta.annotation.security.PermitAll;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Tag(name = "管理后台 - 验证码")
@Path("/system/captcha")
public class CaptchaController {

	@Context
	HttpServerRequest request;

//    @Resource
//    private CaptchaService captchaService;

    @POST
    @Path("/get")
    @Operation(summary = "获得验证码")
    @PermitAll
    public Response get(CaptchaVO data) {
        data.setBrowserInfo(getRemoteId());
        return getCaptcha();
    }
//
//    @PostMapping("/check")
//    @Operation(summary = "校验验证码")
//    @PermitAll
//    @OperateLog(enable = false) // 避免 Post 请求被记录操作日志
//    public ResponseModel check(@RequestBody CaptchaVO data, HttpServletRequest request) {
//        data.setBrowserInfo(getRemoteId(request));
//        return captchaService.check(data);
//    }

	public String getRemoteId() {
		return IpUtil.getSingleIp(request) + request.getHeader("user-agent");
	}

	protected Font waterMarkFont;
	protected static String waterMark = "xingyuv";
	
	ObjectMapper objectMapper = new ObjectMapper();

	private Response getCaptcha() {
		// 原生图片
		BufferedImage originalImage = ImageUtils.getOriginal();
		if (null == originalImage) {
//            return ResponseModel.errorMsg(RepCodeEnum.API_CAPTCHA_BASEMAP_NULL);
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "底图未初始化成功，请检查路径")).build();
		}
		// 设置水印
		Graphics backgroundGraphics = originalImage.getGraphics();
		int width = originalImage.getWidth();
		int height = originalImage.getHeight();
		backgroundGraphics.setFont(waterMarkFont);
		backgroundGraphics.setColor(Color.white);
		backgroundGraphics.drawString(waterMark, width - getEnOrChLength(waterMark), height - (HAN_ZI_SIZE / 2) + 7);

		// 抠图图片
		String jigsawImageBase64 = ImageUtils.getSlidingBlock();
		BufferedImage jigsawImage = ImageUtils.getBase64StrToImage(jigsawImageBase64);
		if (null == jigsawImage) {
//            logger.error("滑动底图未初始化成功，请检查路径");
//			return "底图未初始化成功，请检查路径";
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "底图未初始化成功，请检查路径")).build();
		}
		CaptchaVO captcha = pictureTemplatesCut(originalImage, jigsawImage, jigsawImageBase64);
		if (captcha == null || StringUtils.isBlank(captcha.getJigsawImageBase64())
				|| StringUtils.isBlank(captcha.getOriginalImageBase64())) {
//			return "获取验证码失败,请联系管理员";
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "获取验证码失败,请联系管理员")).build();
		}
//		return captcha;
		return Response.ok().entity(captcha).build();
	}
	protected static int captchaInterferenceOptions = 0;
	protected static final String IMAGE_TYPE_PNG = "png";
	protected static String REDIS_CAPTCHA_KEY = "RUNNING:CAPTCHA:%s";
	protected static String cacheType = "local";
	protected static Long EXPIRESIN_SECONDS = 2 * 60L;
	
	public CaptchaVO pictureTemplatesCut(BufferedImage originalImage, BufferedImage jigsawImage,
			String jigsawImageBase64) {
		try {
			CaptchaVO dataVO = new CaptchaVO();

			int originalWidth = originalImage.getWidth();
			int originalHeight = originalImage.getHeight();
			int jigsawWidth = jigsawImage.getWidth();
			int jigsawHeight = jigsawImage.getHeight();

			// 随机生成拼图坐标
			PointVO point = generateJigsawPoint(originalWidth, originalHeight, jigsawWidth, jigsawHeight);
			int x = point.getX();

			// 生成新的拼图图像
			BufferedImage newJigsawImage = new BufferedImage(jigsawWidth, jigsawHeight, jigsawImage.getType());
			Graphics2D graphics = newJigsawImage.createGraphics();

			int bold = 5;
			// 如果需要生成RGB格式，需要做如下配置,Transparency 设置透明
			newJigsawImage = graphics.getDeviceConfiguration().createCompatibleImage(jigsawWidth, jigsawHeight,
					Transparency.TRANSLUCENT);
			// 新建的图像根据模板颜色赋值,源图生成遮罩
			cutByTemplate(originalImage, jigsawImage, newJigsawImage, x, 0);
			if (captchaInterferenceOptions > 0) {
				int position = 0;
				if (originalWidth - x - 5 > jigsawWidth * 2) {
					// 在原扣图右边插入干扰图
					position = RandomUtils.getRandomInt(x + jigsawWidth + 5, originalWidth - jigsawWidth);
				} else {
					// 在原扣图左边插入干扰图
					position = RandomUtils.getRandomInt(100, x - jigsawWidth - 5);
				}
				while (true) {
					String s = ImageUtils.getSlidingBlock();
					if (!jigsawImageBase64.equals(s)) {
						interferenceByTemplate(originalImage, Objects.requireNonNull(ImageUtils.getBase64StrToImage(s)),
								position, 0);
						break;
					}
				}
			}
			if (captchaInterferenceOptions > 1) {
				while (true) {
					String s = ImageUtils.getSlidingBlock();
					if (!jigsawImageBase64.equals(s)) {
						Integer randomInt = RandomUtils.getRandomInt(jigsawWidth, 100 - jigsawWidth);
						interferenceByTemplate(originalImage, Objects.requireNonNull(ImageUtils.getBase64StrToImage(s)),
								randomInt, 0);
						break;
					}
				}
			}

			// 设置“抗锯齿”的属性
			graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			graphics.setStroke(new BasicStroke(bold, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL));
			graphics.drawImage(newJigsawImage, 0, 0, null);
			graphics.dispose();

			ByteArrayOutputStream os = new ByteArrayOutputStream();// 新建流。
			ImageIO.write(newJigsawImage, IMAGE_TYPE_PNG, os);// 利用ImageIO类提供的write方法，将bi以png图片的数据模式写入流。
			byte[] jigsawImages = os.toByteArray();

			ByteArrayOutputStream oriImagesOs = new ByteArrayOutputStream();// 新建流。
			ImageIO.write(originalImage, IMAGE_TYPE_PNG, oriImagesOs);// 利用ImageIO类提供的write方法，将bi以jpg图片的数据模式写入流。
			byte[] oriCopyImages = oriImagesOs.toByteArray();
			Base64.Encoder encoder = Base64.getEncoder();
			dataVO.setOriginalImageBase64(encoder.encodeToString(oriCopyImages).replaceAll("\r|\n", ""));
			// point信息不传到前端，只做后端check校验
//            dataVO.setPoint(point);
			dataVO.setJigsawImageBase64(encoder.encodeToString(jigsawImages).replaceAll("\r|\n", ""));
			dataVO.setToken(RandomUtils.getUUID());
			dataVO.setSecretKey(point.getSecretKey());
//            base64StrToImage(encoder.encodeToString(oriCopyImages), "D:\\原图.png");
//            base64StrToImage(encoder.encodeToString(jigsawImages), "D:\\滑动.png");

			// 将坐标信息存入redis中
//			String codeKey = String.format(REDIS_CAPTCHA_KEY, dataVO.getToken());
			
//			CaptchaServiceFactory.getCache(cacheType).set(codeKey, objectMapper.writeValueAsString(point), EXPIRESIN_SECONDS);
//			logger.debug("token：{},point:{}", dataVO.getToken(), JsonUtil.toJSONString(point));
			return dataVO;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	private static void interferenceByTemplate(BufferedImage oriImage, BufferedImage templateImage, int x, int y) {
        //临时数组遍历用于高斯模糊存周边像素值
        int[][] martrix = new int[3][3];
        int[] values = new int[9];

        int xLength = templateImage.getWidth();
        int yLength = templateImage.getHeight();
        // 模板图像宽度
        for (int i = 0; i < xLength; i++) {
            // 模板图片高度
            for (int j = 0; j < yLength; j++) {
                // 如果模板图像当前像素点不是透明色 copy源文件信息到目标图片中
                int rgb = templateImage.getRGB(i, j);
                if (rgb < 0) {
                    //抠图区域高斯模糊
                    readPixel(oriImage, x + i, y + j, values);
                    fillMatrix(martrix, values);
                    oriImage.setRGB(x + i, y + j, avgMatrix(martrix));
                }
                //防止数组越界判断
                if (i == (xLength - 1) || j == (yLength - 1)) {
                    continue;
                }
                int rightRgb = templateImage.getRGB(i + 1, j);
                int downRgb = templateImage.getRGB(i, j + 1);
                //描边处理，,取带像素和无像素的界点，判断该点是不是临界轮廓点,如果是设置该坐标像素是白色
                if ((rgb >= 0 && rightRgb < 0) || (rgb < 0 && rightRgb >= 0) || (rgb >= 0 && downRgb < 0) || (rgb < 0 && downRgb >= 0)) {
                    oriImage.setRGB(x + i, y + j, Color.white.getRGB());
                }
            }
        }

    }
	private static void cutByTemplate(BufferedImage oriImage, BufferedImage templateImage, BufferedImage newImage, int x, int y) {
        //临时数组遍历用于高斯模糊存周边像素值
        int[][] martrix = new int[3][3];
        int[] values = new int[9];

        int xLength = templateImage.getWidth();
        int yLength = templateImage.getHeight();
        // 模板图像宽度
        for (int i = 0; i < xLength; i++) {
            // 模板图片高度
            for (int j = 0; j < yLength; j++) {
                // 如果模板图像当前像素点不是透明色 copy源文件信息到目标图片中
                int rgb = templateImage.getRGB(i, j);
                if (rgb < 0) {
                    newImage.setRGB(i, j, oriImage.getRGB(x + i, y + j));

                    //抠图区域高斯模糊
                    readPixel(oriImage, x + i, y + j, values);
                    fillMatrix(martrix, values);
                    oriImage.setRGB(x + i, y + j, avgMatrix(martrix));
                }

                //防止数组越界判断
                if (i == (xLength - 1) || j == (yLength - 1)) {
                    continue;
                }
                int rightRgb = templateImage.getRGB(i + 1, j);
                int downRgb = templateImage.getRGB(i, j + 1);
                //描边处理，,取带像素和无像素的界点，判断该点是不是临界轮廓点,如果是设置该坐标像素是白色
                boolean exists = (rgb >= 0 && rightRgb < 0) ||
                        (rgb < 0 && rightRgb >= 0) ||
                        (rgb >= 0 && downRgb < 0) ||
                        (rgb < 0 && downRgb >= 0);
                if (exists) {
                    newImage.setRGB(i, j, Color.white.getRGB());
                    oriImage.setRGB(x + i, y + j, Color.white.getRGB());
                }
            }
        }

    }
	private static int avgMatrix(int[][] matrix) {
        int r = 0;
        int g = 0;
        int b = 0;
        for (int[] x : matrix) {
            for (int j = 0; j < x.length; j++) {
                if (j == 1) {
                    continue;
                }
                Color c = new Color(x[j]);
                r += c.getRed();
                g += c.getGreen();
                b += c.getBlue();
            }
        }
        return new Color(r / 8, g / 8, b / 8).getRGB();
    }
	private static void fillMatrix(int[][] matrix, int[] values) {
        int filled = 0;
        for (int[] x : matrix) {
            for (int j = 0; j < x.length; j++) {
                x[j] = values[filled++];
            }
        }
    }
	private static void readPixel(BufferedImage img, int x, int y, int[] pixels) {
        int xStart = x - 1;
        int yStart = y - 1;
        int current = 0;
        for (int i = xStart; i < 3 + xStart; i++) {
            for (int j = yStart; j < 3 + yStart; j++) {
                int tx = i;
                if (tx < 0) {
                    tx = -tx;

                } else if (tx >= img.getWidth()) {
                    tx = x;
                }
                int ty = j;
                if (ty < 0) {
                    ty = -ty;
                } else if (ty >= img.getHeight()) {
                    ty = y;
                }
                pixels[current++] = img.getRGB(tx, ty);

            }
        }
    }
	protected static Boolean captchaAesStatus = true;
	private static PointVO generateJigsawPoint(int originalWidth, int originalHeight, int jigsawWidth, int jigsawHeight) {
        Random random = new Random();
        int widthDifference = originalWidth - jigsawWidth;
        int heightDifference = originalHeight - jigsawHeight;
        int x, y = 0;
        if (widthDifference <= 0) {
            x = 5;
        } else {
            x = random.nextInt(originalWidth - jigsawWidth - 100) + 100;
        }
        if (heightDifference <= 0) {
            y = 5;
        } else {
            y = random.nextInt(originalHeight - jigsawHeight) + 5;
        }
        String key = null;
        if (captchaAesStatus) {
            key = AESUtil.getKey();
        }
        return new PointVO(x, y, key);
    }

	protected static int HAN_ZI_SIZE = 25;

	protected static int getEnOrChLength(String s) {
		int enCount = 0;
		int chCount = 0;
		for (int i = 0; i < s.length(); i++) {
			int length = String.valueOf(s.charAt(i)).getBytes(StandardCharsets.UTF_8).length;
			if (length > 1) {
				chCount++;
			} else {
				enCount++;
			}
		}
		int chOffset = (HAN_ZI_SIZE / 2) * chCount + 5;
		int enOffset = enCount * 8;
		return chOffset + enOffset;
	}
}
