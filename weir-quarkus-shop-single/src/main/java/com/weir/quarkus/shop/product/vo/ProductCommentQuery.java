package com.weir.quarkus.shop.product.vo;

import lombok.*;

import java.util.Date;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.weir.quarkus.shop.vo.BaseQuery;

import jakarta.ws.rs.QueryParam;

/**
 * 
 * @Title: ProductComment.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品评论
 * @author weir
 * @date 2023年12月12日 10:05:21 
 * @version V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductCommentQuery extends BaseQuery {

    private Integer id;

    @QueryParam(value = "userNickname")
    @Schema(description = "评价人名称", example = "王二狗")
    private String userNickname;

    @QueryParam(value = "orderId")
    @Schema(description = "交易订单编号", example = "24428")
    private Integer orderId;

    @QueryParam(value = "spuId")
    @Schema(description = "商品SPU编号", example = "29502")
    private Integer spuId;

    @QueryParam(value = "spuName")
    @Schema(description = "商品SPU名称", example = "感冒药")
    private String spuName;

    @QueryParam(value = "scores")
    @Schema(description = "评分星级 1-5 分", example = "5")
    private Integer scores;

    @QueryParam(value = "replyStatus")
    @Schema(description = "商家是否回复", example = "true")
    private Boolean replyStatus;

    @QueryParam(value = "createTime")
    @Schema(description = "创建时间")
    private Date createTime;

}
