package com.weir.quarkus.shop.product.vo;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.weir.quarkus.shop.vo.BaseQuery;

import jakarta.ws.rs.QueryParam;
import lombok.*;

/**
 * 
 * @Title: ProductBrandVo.java 
 * @Package com.weir.quarkus.shop.product.vo 
 * @Description: 商品品牌VO
 * @author weir
 * @date 2023年12月12日 15:22:14 
 * @version V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "商品品牌VO")
public class ProductBrandQuery extends BaseQuery {

    private Integer id;
    /**
     * 品牌名称
     */
    @QueryParam(value = "name")
    private String name;
    /**
     * 状态（0关闭1开启）
     *
     */
    @QueryParam(value = "status")
    private Integer status;

}
