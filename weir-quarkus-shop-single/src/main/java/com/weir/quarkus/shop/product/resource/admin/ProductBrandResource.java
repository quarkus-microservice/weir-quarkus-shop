package com.weir.quarkus.shop.product.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.Comparator;
import java.util.List;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductBrand;
import com.weir.quarkus.shop.product.vo.ProductBrandQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 商品品牌")
@Path("/product/brand")
public class ProductBrandResource {

	@Inject
	EntityManager em;
	
	@POST
	@Path("/create")
    @Operation(summary = "创建品牌")
//    @PreAuthorize("@ss.hasPermission('product:brand:create')")
	@Transactional
    public Response createBrand(@Valid ProductBrand brand) {
		if (ProductBrand.checkName(brand.getName())) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该品牌名称已存在")).build();
		}
		brand.persist();
		return Response.ok().entity(new ResultDataVo<>(200, "创建品牌成功" , brand)).build();
    }

	@PUT
    @Path("/update")
    @Operation(summary = "更新品牌")
	@Transactional
//    @PreAuthorize("@ss.hasPermission('product:brand:update')")
    public Response updateBrand(@Valid ProductBrand brand) {
		if (brand.getId() == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "品牌ID不能为空")).build();
		}
		ProductBrand brandOld = ProductBrand.findById(brand.getId());
		if (brandOld == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该品牌ID不存在")).build();
		}
		if (!brand.getName().equals(brandOld.getName())) {
			if (ProductBrand.checkName(brand.getName())) {
				return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该品牌名称已存在")).build();
			}
		}
		brandOld.setDescription(brand.getDescription());
		brandOld.setPicUrl(brand.getPicUrl());
		brandOld.setSort(brand.getSort());
		brandOld.setStatus(brand.getStatus());
		return Response.ok().entity(new ResultDataVo<>(200, "更新品牌成功" , brandOld)).build();
    }

	@DELETE
    @Path("/delete")
    @Operation(summary = "删除品牌")
	@Transactional
//    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('product:brand:delete')")
    public Response deleteBrand(@QueryParam("id") Integer id) {
        ProductBrand.deleteById(id);
        return Response.ok().entity(new ResultDataVo<>(200, "删除品牌成功" , id)).build();
    }

	@GET
    @Path("/get")
    @Operation(summary = "获得品牌")
//    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('product:brand:query')")
    public Response getBrand(@QueryParam("id") Integer id) {
		ProductBrand brand = ProductBrand.findById(id);
		return Response.ok().entity(new ResultDataVo<>(200, brand)).build();
    }

	@GET
    @Path("/list-all-simple")
    @Operation(summary = "获取品牌精简信息列表", description = "主要用于前端的下拉选项")
    public Response getSimpleBrandList() {
		List<ProductBrand> list = ProductBrand.list("status", 0);
		return Response.ok().entity(new ResultDataVo<>(200, list)).build();
    }

	@GET
    @Path("/page")
    @Operation(summary = "获得品牌分页")
//    @PreAuthorize("@ss.hasPermission('product:brand:query')")
    public Response getBrandPage(@BeanParam ProductBrandQuery query) {
		VuePageListVo<ProductBrand> queryPage = QueryHelper.createQueryPage(em, ProductBrand.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPage)).build();
    }

	@GET
    @Path("/list")
    @Operation(summary = "获得品牌列表")
//    @PreAuthorize("@ss.hasPermission('product:brand:query')")
    public Response getBrandList(@BeanParam ProductBrandQuery query) {
		List<ProductBrand> list = QueryHelper.createQueryReturn(em, ProductBrand.class, query).getResultList();
		list.sort(Comparator.comparing(ProductBrand::getSort));
		return Response.ok().entity(new ResultDataVo<>(200, list)).build();
    }

}
