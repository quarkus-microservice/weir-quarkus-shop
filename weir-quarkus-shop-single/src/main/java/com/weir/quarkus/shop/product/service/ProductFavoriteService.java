/** 
 * @Title: ProductFavoriteService.java 
 * @Package com.weir.quarkus.shop.product.service 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年2月1日 15:20:58 
 * @version V1.0 
 */
package com.weir.quarkus.shop.product.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.weir.quarkus.shop.product.entity.ProductFavorite;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.mapper.ProductFavoriteMapper;
import com.weir.quarkus.shop.product.vo.ProductFavoriteQuery;
import com.weir.quarkus.shop.product.vo.ProductFavoriteVo;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.VuePageListVo;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

/** 
 * @Title: ProductFavoriteService.java 
 * @Package com.weir.quarkus.shop.product.service 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年2月1日 15:20:58 
 * @version V1.0 
 */
@ApplicationScoped
public class ProductFavoriteService {

	@Inject
	EntityManager em;
	
	@Inject
	ProductFavoriteMapper productFavoriteMapper;
	
	public VuePageListVo queryPage(ProductFavoriteQuery query) {
		VuePageListVo<ProductFavorite> queryPage = QueryHelper.createQueryPage(em, ProductFavorite.class, query);
		List<ProductFavorite> favorites = queryPage.getList();
		VuePageListVo vuePageListVo = new VuePageListVo<>();
		if (CollectionUtils.isEmpty(favorites)) {
			return vuePageListVo;
		}
		List<Integer> spuIds = favorites.stream().map(ProductFavorite::getSpuId).collect(Collectors.toList());
		List<ProductSpu> spus = ProductSpu.list("id in (?1)", spuIds);
		Map<Integer, ProductSpu> spuIdMap = spus.stream()
				.collect(Collectors.toMap(ProductSpu::getId, a -> a, (k1, k2) -> k1));
		List<ProductFavoriteVo> fVos = new ArrayList<>();
		for (ProductFavorite productFavorite : favorites) {
			ProductSpu spu = spuIdMap.get(productFavorite.getSpuId());
			ProductFavoriteVo vo = productFavoriteMapper.toVo(spu);
			vo.setId(productFavorite.getId());
			vo.setUserId(productFavorite.getUserId());
			vo.setSpuId(spu.getId());
			fVos.add(vo);
		}
		
		vuePageListVo.setList(fVos);
		vuePageListVo.setPage(queryPage.getPage());
		vuePageListVo.setPageCount(queryPage.getPageCount());
		vuePageListVo.setPageSize(queryPage.getPageSize());
		vuePageListVo.setTotal(queryPage.getTotal());
		return vuePageListVo;
	}
}
