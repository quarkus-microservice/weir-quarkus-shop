package com.weir.quarkus.shop.marketing.vo;

import lombok.*;

import java.time.LocalDateTime;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * 砍价记录 DO TODO
 *
 * @author HUIHUI
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BargainRecordVO {

    /**
     * 编号
     */
    private Integer id;
    /**
     * 用户编号
     */
    private Integer userId;

    /**
     * 砍价活动编号
     *
     * 关联 {@link BargainActivity#getId()} 字段
     */
    private Integer activityId;
    /**
     * 商品 SPU 编号
     */
    private Integer spuId;
    /**
     * 商品 SKU 编号
     */
    private Integer skuId;

    /**
     * 砍价起始价格，单位：分
     */
    private Integer bargainFirstPrice;
    /**
     * 当前砍价，单位：分
     */
    private Integer bargainPrice;

    /**
     * 砍价状态
     *
     * 砍价成功的条件是：（2 选 1）
     *  1. 砍价到 {@link BargainActivity#getBargainMinPrice()} 底价
     *  2. 助力人数到达 {@link BargainActivity#getHelpMaxCount()} 人
     *
     * 枚举 {@link BargainRecordStatusEnum}
     */
    private Integer status;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    /**
     * 订单编号
     */
    private Integer orderId;
    
    @Schema(description = "用户昵称", example = "老芋艿")
    private String nickname;

    @Schema(description = "用户头像", example = "https://www.iocoder.cn/xxx.jpg")
    private String avatar;

    private BargainActivityVO activity;
}
