package com.weir.quarkus.shop.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

/**
 * 
 * @Title: PageResultVo.java 
 * @Package com.weir.quarkus.shop.vo 
 * @Description: 分页结果
 * @author weir
 * @date 2023年12月12日 14:58:10 
 * @version V1.0
 */
@Schema(description = "分页结果")
@Data
public final class PageResultVo<T> implements Serializable {

    private static final long serialVersionUID = -318511672404149667L;

	private List<T> list;

    private Long total;

    public PageResultVo() {
    }

    public PageResultVo(List<T> list, Long total) {
        this.list = list;
        this.total = total;
    }

    public PageResultVo(Long total) {
        this.list = new ArrayList<>();
        this.total = total;
    }

    public static <T> PageResultVo<T> empty() {
        return new PageResultVo<>(0L);
    }

    public static <T> PageResultVo<T> empty(Long total) {
        return new PageResultVo<>(total);
    }

}
