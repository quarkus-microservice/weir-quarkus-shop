package com.weir.quarkus.shop.vo;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import lombok.Data;

/**
 * 
 * @Title: VuePageListVo.java 
 * @Package com.weir.quarkus.shop.vo 
 * @Description: 分页数据
 * @author weir
 * @date 2023年12月12日 15:07:32 
 * @version V1.0
 */
@Schema(description = "分页结果")
@Data
public class VuePageListVo<T> {

	private List<T> list;
	private Integer page = 1;
	private Integer pageSize = 10;
	private Long pageCount = 0L;
	
    private Long total;

	public VuePageListVo(List<T> list, Integer page, Integer pageSize, Long pageCount, Long total) {
		super();
		this.list = list;
		this.page = page;
		this.pageSize = pageSize;
		this.pageCount = pageCount;
		this.total = total;
	}

	public VuePageListVo() {
		super();
	}
	
}
