package com.weir.quarkus.shop.marketing.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;
import java.util.List;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.weir.quarkus.shop.vo.BaseQuery;

/**
 * 优惠劵 DO
 *
 * @author 芋道源码
 */
@Data
public class CouponDTO {

    // ========== 基本信息 BEGIN ==========
    /**
     * 优惠劵编号
     */
    private Integer id;
    /**
     * 优惠劵模板编号
     *
     * 关联 {@link CouponTemplate#getId()}
     */
    private Integer templateId;
    /**
     * 优惠劵名
     *
     * 冗余 {@link CouponTemplate#getName()}
     */
    private String name;
    /**
     * 优惠码状态
     *
     * 枚举 {@link CouponStatusEnum}
     */
    private Integer status;

    // ========== 基本信息 END ==========

    // ========== 领取情况 BEGIN ==========
    /**
     * 用户编号
     *
     * 关联 MemberUserDO 的 id 字段
     */
    private Integer userId;
    /**
     * 领取类型
     *
     * 枚举 {@link CouponTakeTypeEnum}
     */
    private Integer takeType;
    // ========== 领取情况 END ==========

    // ========== 使用规则 BEGIN ==========
    /**
     * 是否设置满多少金额可用，单位：分
     *
     * 冗余 {@link CouponTemplate#getUsePrice()}
     */
    private Integer usePrice;
    /**
     * 生效开始时间
     */
    private LocalDateTime validStartTime;
    /**
     * 生效结束时间
     */
    private LocalDateTime validEndTime;
    /**
     * 商品范围
     *
     * 枚举 {@link PromotionProductScopeEnum}
     */
    private Integer productScope;
    /**
     * 商品范围编号的数组
     *
     * 冗余 {@link CouponTemplate#getProductScopeValues()}
     */
    private String productScopeValues;
    // ========== 使用规则 END ==========

    // ========== 使用效果 BEGIN ==========
    /**
     * 折扣类型
     *
     * 冗余 {@link CouponTemplate#getDiscountType()}
     */
    private Integer discountType;
    /**
     * 折扣百分比
     *
     * 冗余 {@link CouponTemplate#getDiscountPercent()}
     */
    private Integer discountPercent;
    /**
     * 优惠金额，单位：分
     *
     * 冗余 {@link CouponTemplate#getDiscountPrice()}
     */
    private Integer discountPrice;
    /**
     * 折扣上限，仅在 {@link #discountType} 等于 {@link PromotionDiscountTypeEnum#PERCENT} 时生效
     *
     * 冗余 {@link CouponTemplate#getDiscountLimitPrice()}
     */
    private Integer discountLimitPrice;
    // ========== 使用效果 END ==========

    // ========== 使用情况 BEGIN ==========
    /**
     * 使用订单号
     */
    private Integer useOrderId;
    /**
     * 使用时间
     */
    private LocalDateTime useTime;

    // ========== 使用情况 END ==========

    @Schema(description = "用户昵称", example = "老芋艿")
    private String nickname;
}
