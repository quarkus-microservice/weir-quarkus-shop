package com.weir.quarkus.shop.marketing.resource.admin;


import jakarta.annotation.Resource;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.shop.marketing.entity.CombinationActivity;
import com.weir.quarkus.shop.marketing.entity.DiscountActivity;
import com.weir.quarkus.shop.marketing.entity.DiscountProduct;
import com.weir.quarkus.shop.marketing.entity.QCombinationRecord;
import com.weir.quarkus.shop.marketing.entity.QDiscountActivity;
import com.weir.quarkus.shop.marketing.entity.QDiscountProduct;
import com.weir.quarkus.shop.marketing.mapper.DiscountActivityMapper;
import com.weir.quarkus.shop.marketing.vo.CombinationActivityDTO;
import com.weir.quarkus.shop.marketing.vo.DiscountActivityDTO;
import com.weir.quarkus.shop.marketing.vo.DiscountActivityQuery;
import com.weir.quarkus.shop.marketing.vo.DiscountProductDTO;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;


@Tag(name = "管理后台 - 限时折扣活动")
@Path("/promotion/discount-activity")
public class DiscountActivityResource {

	private static final QDiscountActivity qDiscountActivity = QDiscountActivity.discountActivity;
	private static final QDiscountProduct qDiscountProduct = QDiscountProduct.discountProduct;
	
	@Inject EntityManager em;
	@Inject DiscountActivityMapper discountActivityMapper;
	@Inject JPAQueryFactory jpaQueryFactory;
    @POST
    @Path("/create")
    @Operation(summary = "创建限时折扣活动")
//    @PreAuthorize("@ss.hasPermission('promotion:discount-activity:create')")
    public Response createDiscountActivity(DiscountActivityDTO dto) {
    	DiscountActivity discountActivity = discountActivityMapper.toEntity(dto);
    	discountActivity.setStatus(LocalDateTime.now().isAfter(discountActivity.getEndTime()) ? 0:1);
    	discountActivity.persist();
    	List<DiscountProduct> discountProducts = discountActivityMapper.toProductEntity(dto.getProducts());
    	discountProducts.forEach(d->{
    		d.setActivityId(discountActivity.getId());
    	});
    	DiscountProduct.persist(discountProducts);
    	return Response.ok().entity(new ResultDataVo<>(200, "创建限时折扣活动成功", discountActivity)).build();
    }

    @PUT
    @Path("/update")
    @Operation(summary = "更新限时折扣活动")
//    @PreAuthorize("@ss.hasPermission('promotion:discount-activity:update')")
    public Response updateDiscountActivity(DiscountActivityDTO dto) {
    	DiscountActivity old = DiscountActivity.findById(dto.getId());
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "限时折扣活动不存在")).build();
		}
    	if (old.getStatus() == 1) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "限时折扣活动已关闭，不能修改")).build();
		}
    	DiscountActivity discountActivity = discountActivityMapper.toEntity(dto);
    	discountActivity.setStatus(LocalDateTime.now().isAfter(discountActivity.getEndTime()) ? 0:1);
    	discountActivity.persist();
    	
    	List<DiscountProductDTO> productDTOs = dto.getProducts();
    	List<Integer> skuIds = productDTOs.stream().map(DiscountProductDTO::getSkuId).distinct().collect(Collectors.toList());
    	List<Integer> activityIds = jpaQueryFactory.select(qDiscountProduct.activityId)
    	.from(qDiscountProduct)
    	.join(qDiscountActivity).on(qDiscountProduct.activityId.eq(qDiscountActivity.id))
    	.where(qDiscountActivity.status.eq(0)
    			.and(qDiscountProduct.skuId.in(skuIds))
    			.and(qDiscountActivity.startTime.loe(LocalDateTime.now()))
    			.and(qDiscountActivity.endTime.goe(LocalDateTime.now()))).fetch();
    	if (CollectionUtils.isNotEmpty(activityIds)) {
    		activityIds = activityIds.stream().distinct().collect(Collectors.toList());
    		if (activityIds.size() == 1) {
    			if (!activityIds.get(0).equals(dto.getId())) {
    				return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "存在商品参加了其它限时折扣活动")).build();
				}
    		}else {
    			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "存在商品参加了其它限时折扣活动")).build();				
			}
		}
    	List<DiscountProduct> discountProducts = discountActivityMapper.toProductEntity(productDTOs);
    	discountProducts.forEach(d->{
    		d.setActivityId(discountActivity.getId());
    	});
    	DiscountProduct.delete("activityId", dto.getId());
    	DiscountProduct.persist(discountProducts);
    	return Response.ok().entity(new ResultDataVo<>(200, "更新限时折扣活动成功", discountActivity)).build();
    }

    @PUT
    @Path("/close")
    @Operation(summary = "关闭限时折扣活动")
    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:discount-activity:close')")
    public Response closeRewardActivity(@QueryParam("id") Integer id) {
    	DiscountActivity old = DiscountActivity.findById(id);
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "限时折扣活动不存在")).build();
		}
    	if (old.getStatus() == 1) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "限时折扣活动已关闭，不能修改")).build();
		}
    	old.setStatus(1);
    	return Response.ok().entity(new ResultDataVo<>(200, "关闭限时折扣活动成功", old)).build();
    }

    @DELETE
    @Path("/delete")
    @Operation(summary = "删除限时折扣活动")
    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:discount-activity:delete')")
    public Response deleteDiscountActivity(@QueryParam("id") Integer id) {
    	DiscountActivity old = DiscountActivity.findById(id);
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "限时折扣活动不存在")).build();
		}
    	if (old.getStatus() == 0) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "限时折扣活动未关闭，不能删除")).build();
		}
    	old.delete();
    	return Response.ok().entity(new ResultDataVo<>(200, "删除限时折扣活动成功", id)).build();
    }

    @GET
    @Path("/get")
    @Operation(summary = "获得限时折扣活动")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('promotion:discount-activity:query')")
    public Response getDiscountActivity(@QueryParam("id") Integer id) {
    	DiscountActivity old = DiscountActivity.findById(id);
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "限时折扣活动不存在")).build();
		}
    	List<DiscountProduct> list = DiscountProduct.list("activityId", id);
    	DiscountActivityDTO dto = discountActivityMapper.toDTO(old);
    	List<DiscountProductDTO> productDTOs = discountActivityMapper.toProductDTO(list);
    	dto.setProducts(productDTOs);
    	return Response.ok().entity(new ResultDataVo<>(200, dto)).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得限时折扣活动分页")
//    @PreAuthorize("@ss.hasPermission('promotion:discount-activity:query')")
    public Response getDiscountActivityPage(@Valid DiscountActivityQuery query) {
    	VuePageListVo<DiscountActivity> queryPage = QueryHelper.createQueryPage(em, DiscountActivity.class,
				query);
        List<DiscountActivity> list = queryPage.getList();
        if (CollectionUtils.isEmpty(list)) {
        	return Response.ok().entity(new ResultDataVo<>(200, null)).build();
		}
        List<DiscountActivityDTO> dtos = discountActivityMapper.toDTO(list);
        List<Integer> ids = dtos.stream().map(DiscountActivityDTO::getId).collect(Collectors.toList());
        List<DiscountProduct> discountProducts = DiscountProduct.list("activityId in (?1)", ids);
        Map<Integer, List<DiscountProduct>> pMap = discountProducts.stream().collect(Collectors.groupingBy(DiscountProduct::getActivityId));
        dtos.forEach(d->{
        	List<DiscountProduct> pList = pMap.get(d.getId());
        	if (CollectionUtils.isNotEmpty(pList)) {				
        		List<DiscountProductDTO> productDTOs = discountActivityMapper.toProductDTO(pList);
        		d.setProducts(productDTOs);
			}
        });
        VuePageListVo<DiscountActivityDTO> queryPageVo = new VuePageListVo<>(dtos,queryPage.getPage(),queryPage.getPageSize(),queryPage.getPageCount(),queryPage.getTotal());
		return Response.ok().entity(new ResultDataVo<>(200, queryPageVo)).build();
    }

}
