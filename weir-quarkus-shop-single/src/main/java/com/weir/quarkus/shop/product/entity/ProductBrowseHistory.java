/** 
 * @Title: ProductBrowseHistory.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年2月2日 10:11:59 
 * @version V1.0 
 */
package com.weir.quarkus.shop.product.entity;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.weir.quarkus.shop.entity.BaseEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;

/** 
 * @Title: ProductBrowseHistory.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年2月2日 10:11:59 
 * @version V1.0 
 */
@Entity
@Table(name = "product_browse_history")
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "商品浏览历史记录")
public class ProductBrowseHistory extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 商品 SPU 编号
     */
	@Column(name = "spu_id")
    private Integer spuId;
    /**
     * 用户编号
     */
	@Column(name = "user_id")
    private Integer userId;
    /**
     * 用户是否删除
     */
	@Column(name = "user_deleted")
    private Boolean userDeleted;
}
