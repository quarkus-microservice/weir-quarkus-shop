package com.weir.quarkus.shop.product.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductBrand;
import com.weir.quarkus.shop.product.entity.ProductCategory;
import com.weir.quarkus.shop.product.entity.ProductSku;
import com.weir.quarkus.shop.product.entity.ProductSkuPropertyValue;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.mapper.ProductSpuMapper;
import com.weir.quarkus.shop.product.vo.ProductSkuPropertyValueVo;
import com.weir.quarkus.shop.product.vo.ProductSkuVo;
import com.weir.quarkus.shop.product.vo.ProductSpuQuery;
import com.weir.quarkus.shop.product.vo.ProductSpuVo;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 商品 SPU")
@Path("/product/spu")
public class ProductSpuResource {

	@Inject
	EntityManager em;
	
	@Inject
	ProductSpuMapper productSpuMapper;

	@POST
	@Path("/create")
	@Operation(summary = "创建商品 SPU")
//    @PreAuthorize("@ss.hasPermission('product:spu:create')")
	@Transactional
	public Response createProductSpu(ProductSpuVo spu) {
		Response checkCategoryBrand = checkCategoryBrand(spu);
		if (checkCategoryBrand != null) {
			return checkCategoryBrand;
		}

		List<ProductSkuVo> skus = initPrices(spu);
		ProductSpu productSpu = productSpuMapper.toSpuEntity(spu);
		productSpu.persist();
		
		for (ProductSkuVo psVo : skus) {
			ProductSku productSku = productSpuMapper.toSkuEntity(psVo);
			productSku.persist();
			for (ProductSkuPropertyValueVo propertyValueVo : psVo.getPropertyValueVos()) {
				ProductSkuPropertyValue skuPropertyValue = new ProductSkuPropertyValue();
				skuPropertyValue.setPropertyId(propertyValueVo.getPropertyId());
				skuPropertyValue.setPropertyValueId(propertyValueVo.getPropertyValueId());
				skuPropertyValue.setSkuId(productSku.getId());
				skuPropertyValue.setSpuId(productSpu.getId());
				skuPropertyValue.persist();
			}
		}
		return Response.ok().entity(new ResultDataVo<>(200 , productSpu)).build();
	}

	private List<ProductSkuVo> initPrices(ProductSpuVo spu) {
		List<ProductSkuVo> skus = spu.getSkus();
		Integer price = skus.stream().min(Comparator.comparing(ProductSkuVo::getPrice)).get().getPrice();
		Integer marketPrice = skus.stream().min(Comparator.comparing(ProductSkuVo::getMarketPrice)).get()
				.getMarketPrice();
		Integer costPrice = skus.stream().min(Comparator.comparing(ProductSkuVo::getCostPrice)).get().getCostPrice();
		int stockTol = skus.stream().mapToInt(ProductSkuVo::getStock).sum();
		spu.setPrice(price);
		spu.setMarketPrice(marketPrice);
		spu.setCostPrice(costPrice);
		spu.setStock(stockTol);
		// 若是 spu 已有状态则不处理
		if (spu.getStatus() == null) {
			// 默认状态为上架
			spu.setStatus(1);
			// 默认商品销量
			spu.setSalesCount(0);
			// 默认商品浏览量
			spu.setBrowseCount(0);
		}
		return skus;
	}

	private Response checkCategoryBrand(ProductSpuVo spu) {
		ProductCategory pc = ProductCategory.findById(spu.getCategoryId());
		if (pc == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品分类不存在")).build();
		}
		if (pc.getStatus() == 1) {
			return Response.status(Status.BAD_REQUEST)
					.entity(new ResultDataVo<>(400, "商品分类" + pc.getName() + " 已禁用，无法使用")).build();
		}

		ProductBrand pb = ProductBrand.findById(spu.getBrandId());
		if (pb == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品品牌不存在")).build();
		}
		if (pb.getStatus() == 1) {
			return Response.status(Status.BAD_REQUEST)
					.entity(new ResultDataVo<>(400, "商品品牌" + pb.getName() + " 已禁用，无法使用")).build();
		}
		return null;
	}

	@PUT
	@Path("/update")
	@Operation(summary = "更新商品 SPU")
//	@PreAuthorize("@ss.hasPermission('product:spu:update')")
	@Transactional
	public Response updateSpu(ProductSpuVo spu) {
		long count = ProductSpu.count("id",spu.getId());
		if (count == 0) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品SPU 不存在")).build();
		}
		Response checkCategoryBrand = checkCategoryBrand(spu);
		if (checkCategoryBrand != null) {
			return checkCategoryBrand;
		}
		List<ProductSkuVo> skus = initPrices(spu);
		ProductSpu productSpu = productSpuMapper.toSpuEntity(spu);
		em.merge(productSpu);
		
		// 更新 sku
		List<Integer> skuIdsNew = skus.stream().filter(p->p.getId() != null).map(ProductSkuVo::getId).collect(Collectors.toList());
		List<ProductSku> productSkus = ProductSku.list("spuId", spu.getId());
		List<Integer> skuIds = productSkus.stream().map(ProductSku::getId).collect(Collectors.toList());
		skuIds.removeAll(skuIdsNew);
		// 需要删除的sku
		if (skuIds != null && !skuIds.isEmpty()) {
			ProductSku.delete("id", skuIds);
		}
		for (ProductSkuVo psVo : skus) {
			ProductSku productSku = productSpuMapper.toSkuEntity(psVo);
			em.merge(productSku);
			
			ProductSkuPropertyValue.delete("spuId", spu.getId());
			
			for (ProductSkuPropertyValueVo propertyValueVo : psVo.getPropertyValueVos()) {
				ProductSkuPropertyValue skuPropertyValue = new ProductSkuPropertyValue();
				skuPropertyValue.setPropertyId(propertyValueVo.getPropertyId());
				skuPropertyValue.setPropertyValueId(propertyValueVo.getPropertyValueId());
				skuPropertyValue.setSkuId(productSku.getId());
				skuPropertyValue.setSpuId(productSpu.getId());
				skuPropertyValue.persist();
			}
		}
		return Response.ok().entity(new ResultDataVo<>(200 , productSpu)).build();
	}

	@PUT
	@Path("/update-status")
	@Operation(summary = "更新商品 SPU Status")
//	@PreAuthorize("@ss.hasPermission('product:spu:update')")
	@Transactional
	public Response updateStatus(ProductSpu spu) {
		if (spu.getStatus() == null || spu.getId() == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品SPU ID或状态 不能为空")).build();
		}
		ProductSpu ps = ProductSpu.findById(spu.getId());
		if (ps == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品SPU 不存在")).build();
		}
		
		ps.setStatus(spu.getStatus());
		return Response.ok().entity(new ResultDataVo<>(200 , ps)).build();
	}

	@DELETE
	@Path("/delete")
	@Operation(summary = "删除商品 SPU")
	@Parameter(name = "id", description = "编号", required = true, example = "1024")
//	@PreAuthorize("@ss.hasPermission('product:spu:delete')")
	@Transactional
	public  Response deleteSpu(@QueryParam("id") Integer id) {
		ProductSpu ps = ProductSpu.findById(id);
		if (ps == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品SPU 不存在")).build();
		}
		if (ps.getStatus() != -1) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品SPU不是回收状态不能删除")).build();
		}
		
		ps.delete();
		ProductSku.delete("spuId", id);
		ProductSkuPropertyValue.delete("spuId", id);
		return Response.ok().entity(new ResultDataVo<>(200 , "删除成功")).build();
	}

	@GET
	@Path("/get-detail")
	@Operation(summary = "获得商品 SPU 明细")
	@Parameter(name = "id", description = "编号", required = true, example = "1024")
//	@PreAuthorize("@ss.hasPermission('product:spu:query')")
	public Response getSpuDetail(@QueryParam("id") Integer id) {
		ProductSpu ps = ProductSpu.findById(id);
		if (ps == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品SPU 不存在")).build();
		}
		List<ProductSku> productSkus = ProductSku.list("spuId", id);
		ProductSpuVo productSpuVo = productSpuMapper.toSpuVo(ps);
		List<ProductSkuPropertyValueVo> resultList = em.createNamedQuery("ProductSkuPropertyValue.listBySpuId", ProductSkuPropertyValueVo.class).getResultList();
		Map<Integer, List<ProductSkuPropertyValueVo>> skuIdMap = resultList.stream().collect(Collectors.groupingBy(ProductSkuPropertyValueVo::getSkuId));
		List<ProductSkuVo> skuVoList = productSpuMapper.toSkuVo(productSkus);
		for (ProductSkuVo productSkuVo : skuVoList) {
			List<ProductSkuPropertyValueVo> list = skuIdMap.get(productSkuVo.getId());
			productSkuVo.setPropertyValueVos(list);
		}
		productSpuVo.setSkus(skuVoList);
		return Response.ok().entity(new ResultDataVo<>(200 , productSpuVo)).build();
	}

	@GET
	@Path("/list-all-simple")
	@Operation(summary = "获得商品 SPU 精简列表")
//	@PreAuthorize("@ss.hasPermission('product:spu:query')")
	public Response getSpuSimpleList() {
		List<ProductSpu> list = ProductSpu.list("status", 1);
		list.sort(Comparator.comparing(ProductSpu::getSort).reversed());
		return Response.ok().entity(new ResultDataVo<>(200 , list)).build();
	}

//	@GET
//	@Path("/list")
//	@Operation(summary = "获得商品 SPU 详情列表")
//	@Parameter(name = "spuIds", description = "spu id逗号分隔", required = true, example = "1,2,3")
////	@PreAuthorize("@ss.hasPermission('product:spu:query')")
//	public Response getSpuList(@QueryParam("spuIds") String spuIds) {
//		return success(ProductSpuConvert.INSTANCE.convertForSpuDetailRespListVO(productSpuService.getSpuList(spuIds),
//				productSkuService.getSkuListBySpuId(spuIds)));
//	}

	@GET
	@Path("/page")
	@Operation(summary = "获得商品 SPU 分页")
//	@PreAuthorize("@ss.hasPermission('product:spu:query')")
	public Response getSpuPage(@BeanParam ProductSpuQuery query) {
		VuePageListVo<ProductSpu> queryPage = QueryHelper.createQueryPage(em, ProductSpu.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, queryPage)).build();
	}

	@GET
	@Path("/get-count")
	@Operation(summary = "获得商品 SPU 分页 tab count")
//	@PreAuthorize("@ss.hasPermission('product:spu:query')")
	public Response getSpuCount() {
		Map<Integer, Long> counts = new HashMap<>();
		// 上架
		long countIn = ProductSpu.count("status", 1);
		// 下架
		long countDown = ProductSpu.count("status", 0);
		// 无效
		long countDel = ProductSpu.count("status", -1);
		// 库存为空
		long count0 = ProductSpu.count("stock", 0);
		// 告警库存 <10
		long countAlert = ProductSpu.count("status = 1 and stock < ?1", 10);
		counts.put(0, countIn);
		counts.put(1, countDown);
		counts.put(2, count0);
		counts.put(3, countAlert);
		counts.put(4, countDel);
		return Response.ok().entity(new ResultDataVo<>(200 , counts)).build();
	}

//	@GetMapping("/export")
//	@Operation(summary = "导出商品")
//	@PreAuthorize("@ss.hasPermission('product:spu:export')")
//	@OperateLog(type = EXPORT)
//	public void exportUserList(@Validated ProductSpuExportReqVO reqVO, HttpServletResponse response)
//			throws IOException {
//		List<ProductSpuDO> spuList = productSpuService.getSpuList(reqVO);
//		// 导出 Excel
//		List<ProductSpuExcelVO> datas = ProductSpuConvert.INSTANCE.convertList03(spuList);
//		ExcelUtils.write(response, "商品列表.xls", "数据", ProductSpuExcelVO.class, datas);
//	}

}
