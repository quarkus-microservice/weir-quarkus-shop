package com.weir.quarkus.shop.product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import com.weir.quarkus.shop.entity.BaseEntity;

import lombok.*;

/**
 * 
 * @Title: ProductBrandDO.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品品牌
 * @author weir
 * @date 2023年12月12日 09:44:25 
 * @version V1.0
 */
@Entity
@Table(name = "product_brand")
@Data
@EqualsAndHashCode(callSuper = true)
@Schema(description = "商品品牌")
public class ProductBrand extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 品牌名称
     */
	@Schema(title = "品牌名称", required = true, example = "苹果")
	@NotBlank(message = "品牌名称不能为空")
    private String name;
    /**
     * 品牌图片
     */
	@Schema(title = "品牌图片", required = true, example = "/xxx/xx.jpg")
	@NotBlank(message = "品牌图片不能为空")
    @Column(name = "pic_url")
    private String picUrl;
    /**
     * 品牌排序
     */
	@Schema(title = "品牌排序", required = true, example = "1")
    @NotNull(message = "品牌排序不能为空")
    private Integer sort;
    /**
     * 品牌描述
     */
    private String description;
    /**
     * 状态（0关闭1开启）
     *
     */
    @Schema(title = "品牌状态", required = true, example = "1")
    @NotNull(message = "品牌状态不能为空")
    private Integer status = 1;

    /**
     * 判断名称是否存在
     * @param name
     * @return
     */
    public static boolean checkName(String name) {
    	long count = ProductBrand.count("name", name);
    	if (count > 0) {
			return true;
		}
    	return false;
	}
}
