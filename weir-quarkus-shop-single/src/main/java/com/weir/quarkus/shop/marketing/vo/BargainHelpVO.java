package com.weir.quarkus.shop.marketing.vo;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import lombok.*;

/**
 * 砍价助力 DO
 *
 * @author HUIHUI
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BargainHelpVO {

    /**
     * 编号
     */
    private Integer id;

    /**
     * 砍价活动编号
     *
     * 关联 {@link BargainActivity#getId()} 字段
     */
    private Integer activityId;
    /**
     * 砍价记录编号
     *
     * 关联 {@link BargainRecordQuery#getId()} 字段
     */
    private Integer recordId;

    /**
     * 用户编号
     */
    private Integer userId;
    /**
     * 减少价格，单位：分
     */
    private Integer reducePrice;
    
    @Schema(description = "用户昵称", example = "weir")
    private String nickname;

    @Schema(description = "用户头像", example = "https://www.iocoder.cn/xxx.jpg")
    private String avatar;

}
