package com.weir.quarkus.shop.marketing.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.shop.marketing.entity.CombinationActivity;
import com.weir.quarkus.shop.marketing.entity.CombinationProduct;
import com.weir.quarkus.shop.marketing.entity.QCombinationRecord;
import com.weir.quarkus.shop.marketing.mapper.CombinationProductMapper;
import com.weir.quarkus.shop.marketing.vo.CombinationActivityDTO;
import com.weir.quarkus.shop.marketing.vo.CombinationActivityQuery;
import com.weir.quarkus.shop.marketing.vo.CombinationProductVO;
import com.weir.quarkus.shop.marketing.vo.CombinationRecordDTO;
import com.weir.quarkus.shop.product.entity.ProductSku;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 拼团活动")
@Path("/promotion/combination-activity")
public class CombinationActivityResource {

	private static final QCombinationRecord qCombinationRecord = QCombinationRecord.combinationRecord;

	@Inject
	CombinationProductMapper combinationProductMapper;
	@Inject
	EntityManager em;
	@Inject
	JPAQueryFactory jpaQueryFactory;

	@POST
	@Path("/create")
	@Operation(summary = "创建拼团活动")
//    @PreAuthorize("@ss.hasPermission('promotion:combination-activity:create')")
	public Response createCombinationActivity(@Valid @RequestBody CombinationActivityDTO dto) {
		ProductSpu spu = ProductSpu.findById(dto.getSpuId());
		if (spu == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SPU 不存在")).build();
		}
		long count = CombinationActivity.count("status = 0 and spuId=?1", dto.getSpuId());
		// 存在商品参加了其它拼团活动
		if (count > 0) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "存在商品参加了其它拼团活动")).build();
		}
		List<ProductSku> skus = ProductSku.list("spuId=?1", dto.getSpuId());
		Map<Integer, ProductSku> skuMap = skus.stream()
				.collect(Collectors.toMap(ProductSku::getId, a -> a, (k1, k2) -> k1));
		List<CombinationProductVO> products = dto.getProducts();
		for (CombinationProductVO p : products) {
			if (!skuMap.containsKey(p.getSkuId())) {
				return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SKU 不存在")).build();
			}
		}
		CombinationActivity combinationActivity = combinationProductMapper.toEntity(dto);
		combinationActivity.setStatus(0);
		combinationActivity.persist();
		for (CombinationProductVO p : products) {
			CombinationProduct combinationProduct = new CombinationProduct();
			combinationProduct.setActivityEndTime(combinationActivity.getEndTime());
			combinationProduct.setActivityStartTime(combinationActivity.getStartTime());
			combinationProduct.setActivityId(combinationActivity.getId());
			combinationProduct.setCombinationPrice(p.getCombinationPrice());
			combinationProduct.setSkuId(p.getSkuId());
			combinationProduct.setSpuId(combinationActivity.getSpuId());
			combinationProduct.persist();
		}
		return Response.ok().entity(new ResultDataVo<>(200, "创建拼团活动成功", combinationActivity)).build();
	}

	@PUT
	@Path("/update")
	@Operation(summary = "更新拼团活动")
//    @PreAuthorize("@ss.hasPermission('promotion:combination-activity:update')")
	public Response updateCombinationActivity(@Valid @RequestBody CombinationActivityDTO dto) {
		CombinationActivity old = CombinationActivity.findById(dto.getId());
		if (old == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "拼团活动不存在")).build();
		}
		if (old.getStatus() == 1) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "拼团活动已关闭不能修改")).build();
		}
		List<CombinationActivity> list = CombinationActivity.list("status = 0 and spuId=?1", dto.getSpuId());
		if (list.size() > 1) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "存在商品参加了其它拼团活动")).build();
		} else if (list.size() == 1) {
			CombinationActivity ca = list.get(0);
			if (!ca.getId().equals(dto.getId())) {
				return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "存在商品参加了其它拼团活动")).build();
			}
		}

		ProductSpu spu = ProductSpu.findById(dto.getSpuId());
		if (spu == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SPU 不存在")).build();
		}
		List<ProductSku> skus = ProductSku.list("spuId=?1", dto.getSpuId());
		Map<Integer, ProductSku> skuMap = skus.stream()
				.collect(Collectors.toMap(ProductSku::getId, a -> a, (k1, k2) -> k1));
		List<CombinationProductVO> products = dto.getProducts();
		for (CombinationProductVO p : products) {
			if (!skuMap.containsKey(p.getSkuId())) {
				return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SKU 不存在")).build();
			}
		}
		CombinationActivity combinationActivity = combinationProductMapper.toEntity(dto);
		combinationActivity.persist();

		CombinationProduct.delete("activityId = ?1", dto.getId());
		for (CombinationProductVO p : products) {
			CombinationProduct combinationProduct = new CombinationProduct();
			combinationProduct.setActivityEndTime(combinationActivity.getEndTime());
			combinationProduct.setActivityStartTime(combinationActivity.getStartTime());
			combinationProduct.setActivityId(combinationActivity.getId());
			combinationProduct.setCombinationPrice(p.getCombinationPrice());
			combinationProduct.setSkuId(p.getSkuId());
			combinationProduct.setSpuId(combinationActivity.getSpuId());
			combinationProduct.persist();
		}
		return Response.ok().entity(new ResultDataVo<>(200, "更新拼团活动成功", combinationActivity)).build();
	}

	@PUT
	@Path("/close")
	@Operation(summary = "关闭拼团活动")
	@Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:combination-activity:close')")
	public Response closeCombinationActivity(@QueryParam("id") Integer id) {
		CombinationActivity old = CombinationActivity.findById(id);
		if (old == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "拼团活动不存在")).build();
		}
		if (old.getStatus() == 1) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "拼团活动已关闭不能修改")).build();
		}
		old.setStatus(1);
		old.persist();
		return Response.ok().entity(new ResultDataVo<>(200, "关闭拼团活动成功", old)).build();
	}

	@DELETE
	@Path("/delete")
	@Operation(summary = "删除拼团活动")
	@Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:combination-activity:delete')")
	public Response deleteCombinationActivity(@QueryParam("id") Integer id) {
		CombinationActivity old = CombinationActivity.findById(id);
		if (old == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "拼团活动不存在")).build();
		}
		if (old.getStatus() == 0) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "拼团活动未关闭或未结束，不能删除")).build();
		}
		CombinationActivity.deleteById(id);
		return Response.ok().entity(new ResultDataVo<>(200, "删除拼团活动成功", id)).build();
	}

	@GET
	@Path("/get")
	@Operation(summary = "获得拼团活动")
	@Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('promotion:combination-activity:query')")
	public Response getCombinationActivity(@QueryParam("id") Integer id) {
		CombinationActivity old = CombinationActivity.findById(id);
		if (old == null) {
			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "拼团活动不存在")).build();
		}
		List<CombinationProduct> combinationProducts = CombinationProduct.list("activityId = ?1", id);
		List<CombinationProductVO> combinationProductVOs = combinationProductMapper.toVo(combinationProducts);
		CombinationActivityDTO dto = combinationProductMapper.toDTO(old);
		dto.setProducts(combinationProductVOs);
		return Response.ok().entity(new ResultDataVo<>(200, dto)).build();
	}

	@GET
	@Path("/page")
	@Operation(summary = "获得拼团活动分页")
//    @PreAuthorize("@ss.hasPermission('promotion:combination-activity:query')")
	public Response getCombinationActivityPage(@Valid CombinationActivityQuery query) {
		VuePageListVo<CombinationActivity> queryPage = QueryHelper.createQueryPage(em, CombinationActivity.class,
				query);
		List<CombinationActivity> list = queryPage.getList();
		List<Integer> ids = list.stream().map(CombinationActivity::getId).collect(Collectors.toList());
		List<Integer> spuIds = list.stream().map(CombinationActivity::getSpuId).collect(Collectors.toList());
		List<CombinationRecordDTO> combinationRecordDTOs = jpaQueryFactory
				.select(Projections.fields(CombinationRecordDTO.class,
						qCombinationRecord.userId.countDistinct().as("recordCount"), qCombinationRecord.activityId))
				.from(qCombinationRecord)
				.where(qCombinationRecord.activityId.in(ids).and(qCombinationRecord.headId.eq(0)))
				.groupBy(qCombinationRecord.activityId).fetch();
		Map<Integer, CombinationRecordDTO> combinationRecordDTOMap = combinationRecordDTOs.stream().collect(Collectors.toMap(CombinationRecordDTO::getActivityId, a -> a, (k1, k2) -> k1));
		List<CombinationRecordDTO> combinationRecordSuccessDTOs = jpaQueryFactory
				.select(Projections.fields(CombinationRecordDTO.class,
						qCombinationRecord.userId.countDistinct().as("recordCount"), qCombinationRecord.activityId))
				.from(qCombinationRecord)
				.where(qCombinationRecord.activityId.in(ids).and(qCombinationRecord.headId.eq(0)).and(qCombinationRecord.status.eq(0)))
				.groupBy(qCombinationRecord.activityId).fetch();
		Map<Integer, CombinationRecordDTO> combinationRecordSuccessDTOMap = combinationRecordSuccessDTOs.stream().collect(Collectors.toMap(CombinationRecordDTO::getActivityId, a -> a, (k1, k2) -> k1));
		List<CombinationRecordDTO> combinationRecordAllDTOs = jpaQueryFactory
				.select(Projections.fields(CombinationRecordDTO.class,
						qCombinationRecord.userId.countDistinct().as("recordCount"), qCombinationRecord.activityId))
				.from(qCombinationRecord)
				.where(qCombinationRecord.activityId.in(ids))
				.groupBy(qCombinationRecord.activityId).fetch();
		Map<Integer, CombinationRecordDTO> combinationRecordAllDTOMap = combinationRecordAllDTOs.stream().collect(Collectors.toMap(CombinationRecordDTO::getActivityId, a -> a, (k1, k2) -> k1));
		List<CombinationProduct> combinationProducts = CombinationProduct.list("activityId in (?1)", ids);
		List<CombinationProductVO> combinationProductVOs = combinationProductMapper.toVo(combinationProducts);
		Map<Integer, List<CombinationProductVO>> combinationProductVOMap = combinationProductVOs.stream().collect(Collectors.groupingBy(CombinationProductVO::getActivityId));
		List<ProductSpu> spus = ProductSpu.list("id in (?1)", spuIds);
		Map<Integer, ProductSpu> spuMap = spus.stream().collect(Collectors.toMap(ProductSpu::getId, a -> a, (k1, k2) -> k1));
		List<CombinationActivityDTO> dtos = combinationProductMapper.toDTO(list);
		dtos.forEach(c->{
			ProductSpu productSpu = spuMap.get(c.getSpuId());
			if (productSpu != null) {
				c.setSpuName(productSpu.getName());
				c.setPicUrl(productSpu.getPicUrl());
				c.setMarketPrice(productSpu.getMarketPrice());
			}
			List<CombinationProductVO> pList = combinationProductVOMap.get(c.getId());
			c.setProducts(pList);
			CombinationRecordDTO combinationRecordDTO = combinationRecordDTOMap.get(c.getId());
			if (combinationRecordDTO != null) {
				c.setGroupCount(combinationRecordDTO.getRecordCount());
			}
			CombinationRecordDTO combinationRecordSuccessDTO = combinationRecordSuccessDTOMap.get(c.getId());
			if (combinationRecordSuccessDTO != null) {
				c.setGroupSuccessCount(combinationRecordSuccessDTO.getRecordCount());
			}
			CombinationRecordDTO combinationRecordAllDTO = combinationRecordAllDTOMap.get(c.getId());
			if (combinationRecordAllDTO != null) {
				c.setRecordCount(combinationRecordAllDTO.getRecordCount());
			}
		});
		VuePageListVo<CombinationActivityDTO> queryPageVo = new VuePageListVo<>(dtos,queryPage.getPage(),queryPage.getPageSize(),queryPage.getPageCount(),queryPage.getTotal());
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPageVo)).build();
	}

}
