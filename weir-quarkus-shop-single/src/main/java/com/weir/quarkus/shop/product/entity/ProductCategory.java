package com.weir.quarkus.shop.product.entity;

import com.weir.quarkus.shop.entity.BaseEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

/**
 * 
 * @Title: ProductCategoryDO.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品分类
 * @author weir
 * @date 2023年12月12日 09:56:32 
 * @version V1.0
 */
@Entity
@Table(name = "product_category")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductCategory extends BaseEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 父分类编号
     */
	@Column(name = "parent_id")
    private Integer parentId;
    /**
     * 分类名称
     */
	@NotBlank(message = "分类名称不能为空")
    private String name;
    /**
     * 移动端分类图
     *
     * 建议 180*180 分辨率
     */
    @Column(name = "pic_url")
    private String picUrl;
    /**
     * PC 端分类图
     *
     * 建议 468*340 分辨率
     */
    @Column(name = "big_pic_url")
    private String bigPicUrl;
    /**
     * 分类排序
     */
    private Integer sort;
    /**
     * 开启状态
     *
     */
    private Integer status = 0;

    /**
     * 判断父id是否存在
     * @param pId
     * @return true 不存在
     */
    public static boolean checkParentIdExist(Integer pId) {
    	if (pId == null){
			return false;
		}
		if (findById(pId).isPersistent()) {
			return false;
		}else {
			return true;
		}
	}
    /**
     * 是否存在子项
     * @param pId
     * @return true 存在
     */
    public static boolean countParentIdExist(Integer pId) {
		if (count("parentId", pId) > 0) {
			return true;
		}
		return false;
	}
}
