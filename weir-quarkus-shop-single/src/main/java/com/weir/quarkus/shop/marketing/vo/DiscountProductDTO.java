package com.weir.quarkus.shop.marketing.vo;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 限时折扣商品 DO
 *
 * @author 芋道源码
 */
@Data
public class DiscountProductDTO {

    /**
     * 编号，主键自增
     */
    private Integer id;

    /**
     * 限时折扣活动的编号
     *
     * 关联 {@link DiscountActivity#getId()}
     */
    private Integer activityId;

    /**
     * 商品 SPU 编号
     *
     * 关联 ProductSpuDO 的 id 编号
     */
    private Integer spuId;
    /**
     * 商品 SKU 编号
     *
     * 关联 ProductSkuDO 的 id 编号
     */
    private Integer skuId;

    /**
     * 折扣类型
     *
     * 枚举 {@link PromotionDiscountTypeEnum}
     */
    private Integer discountType;
    /**
     * 折扣百分比
     *
     * 例如，80% 为 80
     */
    private Integer discountPercent;
    /**
     * 优惠金额，单位：分
     *
     * 当 {@link #discountType} 为 {@link PromotionDiscountTypeEnum#PRICE} 生效
     */
    private Integer discountPrice;

    /**
     * 活动状态
     *
     * 关联 {@link DiscountActivity#getStatus()}
     */
    private Integer activityStatus;
    /**
     * 活动开始时间点
     *
     * 冗余 {@link DiscountActivity#getStartTime()}
     */
    private LocalDateTime activityStartTime;
    /**
     * 活动结束时间点
     *
     * 冗余 {@link DiscountActivity#getEndTime()}
     */
    private LocalDateTime activityEndTime;

}
