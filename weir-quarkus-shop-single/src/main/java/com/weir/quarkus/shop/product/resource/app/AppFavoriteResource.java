package com.weir.quarkus.shop.product.resource.app;


import jakarta.enterprise.inject.spi.CDI;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

import org.eclipse.microprofile.jwt.Claims;
import org.eclipse.microprofile.jwt.JsonWebToken;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductFavorite;
import com.weir.quarkus.shop.product.service.ProductFavoriteService;
import com.weir.quarkus.shop.product.vo.ProductFavoriteQuery;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "用户 APP - 商品收藏")
@Path("app/product/favorite")
public class AppFavoriteResource {

	@Inject
	EntityManager em;
	@Inject
	ProductFavoriteService productFavoriteService;
//	private SysUser getUser(String userJson) {
//		return new JsonObject(new JsonObject(userJson).getValue("user").toString()).mapTo(SysUser.class);
//	}
	
    @POST
    @Path(value = "/create")
    @Operation(summary = "添加商品收藏")
//    @PreAuthenticated
    @Transactional
    public Response createFavorite(@RequestBody @Valid ProductFavorite favorite) {
    	JsonWebToken context = CDI.current().select(JsonWebToken.class).get();
		Object claim = context.getClaim(Claims.preferred_username.name());
//		SysUser user = getUser(claim.toString());
		favorite.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "创建商品收藏成功", favorite)).build();
    }

    @DELETE
    @Path(value = "/delete")
    @Operation(summary = "取消单个商品收藏")
//    @PreAuthenticated
    @Transactional
    public Response deleteFavorite(@QueryParam("id") Integer id) {
    	ProductFavorite.deleteById(id);
    	return Response.ok().entity(new ResultDataVo<>(200, "取消商品收藏成功", true)).build();
    }

    @GET
    @Path(value = "/page")
    @Operation(summary = "获得商品收藏分页")
//    @PreAuthenticated
    public Response getFavoritePage() {
    	ProductFavoriteQuery query = new ProductFavoriteQuery();
//    	query.setUserId(null);
    	VuePageListVo vuePageListVo = productFavoriteService.queryPage(query);
		return Response.ok().entity(new ResultDataVo<>(200, vuePageListVo)).build();
    }

//    @GetMapping(value = "/exits")
//    @Operation(summary = "检查是否收藏过商品")
//    @PreAuthenticated
//    public CommonResult<Boolean> isFavoriteExists(AppFavoriteReqVO reqVO) {
//        ProductFavoriteDO favorite = productFavoriteService.getFavorite(getLoginUserId(), reqVO.getSpuId());
//        return success(favorite != null);
//    }

    @GET
    @Path(value = "/get-count")
    @Operation(summary = "获得商品收藏数量")
//    @PreAuthenticated
    public Response getFavoriteCount() {
    	long count = ProductFavorite.count("userId", 1);
    	return Response.ok().entity(new ResultDataVo<>(200, count)).build();
    }

}
