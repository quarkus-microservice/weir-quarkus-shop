/** 
 * @Title: CouponSendDTO.java 
 * @Package com.weir.quarkus.shop.marketing.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年2月17日 13:50:07 
 * @version V1.0 
 */
package com.weir.quarkus.shop.marketing.vo;

import java.util.Set;

import org.eclipse.microprofile.openapi.annotations.media.Schema;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

/** 
 * @Title: CouponSendDTO.java 
 * @Package com.weir.quarkus.shop.marketing.vo 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author weir
 * @date 2024年2月17日 13:50:07 
 * @version V1.0 
 */
@Data
public class CouponSendDTO {
	@Schema(description = "优惠劵模板编号",  example = "1024")
    @NotNull(message = "优惠劵模板编号不能为空")
    private Integer templateId;

    @Schema(description = "用户编号列表", example = "[1, 2]")
    @NotEmpty(message = "用户编号列表不能为空")
    private Set<Integer> userIds;
}
