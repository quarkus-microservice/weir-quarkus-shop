package com.weir.quarkus.shop.product.vo;

import com.weir.quarkus.shop.vo.BaseQuery;

import jakarta.ws.rs.QueryParam;
import lombok.*;

/**
 * 
 * @Title: ProductProperty.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品属性项
 * @author weir
 * @date 2023年12月12日 10:11:27 
 * @version V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductPropertyQuery extends BaseQuery {

    private Integer id;
    /**
     * 名称
     */
    @QueryParam(value = "name")
    private String name;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;

}
