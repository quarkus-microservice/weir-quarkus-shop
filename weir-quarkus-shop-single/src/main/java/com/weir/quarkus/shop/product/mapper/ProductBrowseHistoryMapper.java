package com.weir.quarkus.shop.product.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.vo.ProductBrowseHistoryVo;

/** 
 * @Title: ProductBrandMapper.java 
 * @Package com.weir.quarkus.shop.product.mapper 
 * @Description: 收藏夹
 * @author weir
 * @date 2023年12月12日 14:12:05 
 * @version V1.0 
 */
@Mapper(componentModel = "jakarta", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ProductBrowseHistoryMapper {

	ProductBrowseHistoryVo toVo(ProductSpu spu);
}
