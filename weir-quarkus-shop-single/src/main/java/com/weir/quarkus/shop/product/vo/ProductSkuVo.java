package com.weir.quarkus.shop.product.vo;

import java.util.List;

import lombok.*;

/**
 * 商品 SKU
 * @Title: ProductSku.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品 SKU
 * @author weir
 * @date 2023年12月13日 15:44:08 
 * @version V1.0
 */
@Data
public class ProductSkuVo {
	
	private List<ProductSkuPropertyValueVo> propertyValueVos;

    private Integer id;
    /**
     * SPU 编号
     *
     */
    private Integer spuId;
    /**
     * 商品价格，单位：分
     */
    private Integer price;
    /**
     * 市场价，单位：分
     */
    private Integer marketPrice;
    /**
     * 成本价，单位：分
     */
    private Integer costPrice;
    /**
     * 商品条码
     */
    private String barCode;
    /**
     * 图片地址
     */
    private String picUrl;
    /**
     * 库存
     */
    private Integer stock;
    /**
     * 商品重量，单位：kg 千克
     */
    private Double weight;
    /**
     * 商品体积，单位：m^3 平米
     */
    private Double volume;

    /**
     * 一级分销的佣金，单位：分
     */
    private Integer firstBrokeragePrice;
    /**
     * 二级分销的佣金，单位：分
     */
    private Integer secondBrokeragePrice;

    /**
     * 商品销量
     */
    private Integer salesCount;

}

