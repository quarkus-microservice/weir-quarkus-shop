package com.weir.quarkus.shop.marketing.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.weir.quarkus.shop.marketing.entity.BargainHelp;
import com.weir.quarkus.shop.marketing.vo.BargainHelpVO;

/** 
 * @Title: ProductBrandMapper.java 
 * @Package com.weir.quarkus.shop.product.mapper 
 * @Description: 收藏夹
 * @author weir
 * @date 2023年12月12日 14:12:05 
 * @version V1.0 
 */
@Mapper(componentModel = "jakarta", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface BargainHelpMapper {

	BargainHelpVO toVo(BargainHelp ba);
	List<BargainHelpVO> toVo(List<BargainHelp> bas);
}
