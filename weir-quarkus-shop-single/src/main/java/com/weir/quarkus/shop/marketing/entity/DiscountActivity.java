package com.weir.quarkus.shop.marketing.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

import com.weir.quarkus.shop.entity.BaseEntity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

/**
 * 限时折扣活动 DO
 *
 * 一个活动下，可以有 {@link DiscountProduct} 商品；
 * 一个商品，在指定时间段内，只能属于一个活动；
 *
 * @author 芋道源码
 */
@Entity
@Table(name = "promotion_discount_activity")
@Data
@EqualsAndHashCode(callSuper = true)
public class DiscountActivity extends BaseEntity {

    /**
     * 活动编号，主键自增
     */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 活动标题
     */
    private String name;
    /**
     * 状态
     *
     * 枚举 {@link CommonStatusEnum}
     *
     * 活动被关闭后，不允许再次开启。
     */
    private Integer status;
    /**
     * 开始时间
     */
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;
    /**
     * 备注
     */
    private String remark;

}
