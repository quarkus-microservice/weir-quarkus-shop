package com.weir.quarkus.shop.marketing.vo;

import lombok.Data;

import java.time.LocalDateTime;

import com.weir.quarkus.shop.vo.BaseQuery;

/**
 * 限时折扣活动 DO
 *
 * 一个活动下，可以有 {@link DiscountProductDTO} 商品；
 * 一个商品，在指定时间段内，只能属于一个活动；
 *
 * @author 芋道源码
 */
@Data
public class DiscountActivityQuery extends BaseQuery {

    /**
     * 活动编号，主键自增
     */
    private Integer id;
    /**
     * 活动标题
     */
    private String name;
    /**
     * 状态
     *
     * 枚举 {@link CommonStatusEnum}
     *
     * 活动被关闭后，不允许再次开启。
     */
    private Integer status;
    /**
     * 开始时间
     */
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;
    /**
     * 备注
     */
    private String remark;

}
