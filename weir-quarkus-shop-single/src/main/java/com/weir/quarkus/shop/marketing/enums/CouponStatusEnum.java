package com.weir.quarkus.shop.marketing.enums;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 优惠劵状态枚举
 *
 * @author 芋道源码
 */
@AllArgsConstructor
@Getter
public enum CouponStatusEnum {

    UNUSED(1, "未使用"),
    USED(2, "已使用"),
    EXPIRE(3, "已过期"),
    ;

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(CouponStatusEnum::getStatus).toArray();

    /**
     * 值
     */
    private final Integer status;
    /**
     * 名字
     */
    private final String name;

    public int[] array() {
        return ARRAYS;
    }

}