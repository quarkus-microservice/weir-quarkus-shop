package com.weir.quarkus.shop.marketing.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

import com.weir.quarkus.shop.entity.BaseEntity;

/**
 * 满减送活动 DO
 *
 * @author 芋道源码
 */
@Entity
@Table(name = "promotion_reward_activity")
@Data
@EqualsAndHashCode(callSuper = true)
public class RewardActivity extends BaseEntity {

    /**
     * 活动编号，主键自增
     */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 活动标题
     */
    private String name;
    /**
     * 状态
     *
     * 枚举 {@link PromotionActivityStatusEnum}
     */
    private Integer status;
    /**
     * 开始时间
     */
    private LocalDateTime startTime;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 条件类型
     *
     * 枚举 {@link PromotionConditionTypeEnum}
     */
    private Integer conditionType;
    /**
     * 商品范围
     *
     * 枚举 {@link PromotionProductScopeEnum}
     */
    private Integer productScope;
    /**
     * 商品 SPU 编号的数组
     */
    private String productSpuIds;
    /**
     * 优惠规则的数组
     */
    private String rules;

}
