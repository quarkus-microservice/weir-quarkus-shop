package com.weir.quarkus.shop.product.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductProperty;
import com.weir.quarkus.shop.product.entity.ProductPropertyValue;
import com.weir.quarkus.shop.product.vo.ProductPropertyQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 商品属性项")
@Path("/product/property")
public class ProductPropertyResource {
	@Inject
	EntityManager em;
	
    @POST
    @Path("/create")
    @Operation(summary = "创建属性项")
//    @PreAuthorize("@ss.hasPermission('product:property:create')")
    @Transactional
    public Response createProperty(ProductProperty property) {
    	ProductProperty p = ProductProperty.find("name", property.getName()).singleResult();
    	if (p != null) {
			return Response.ok().entity(new ResultDataVo<>(200 , p)).build();
		}
    	property.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "创建属性项成功", property)).build();
    }

    @PUT
    @Path("/update")
    @Operation(summary = "更新属性项")
//    @PreAuthorize("@ss.hasPermission('product:property:update')")
    @Transactional
    public Response updateProperty(ProductProperty property) {
    	ProductProperty pOld = ProductProperty.findById(property.getId());
    	if (pOld == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该属性项不存在")).build();
		}
    	if (!property.getName().equals(pOld.getName())) {
    		long count = ProductProperty.count("name", property.getName());
    		if (count > 0) {
    			return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该属性项名称已存在，不能重复")).build();
			}
		}
    	pOld.setName(property.getName());
    	pOld.setStatus(property.getStatus());
    	pOld.setRemark(property.getRemark());
    	return Response.ok().entity(new ResultDataVo<>(200, "创建属性项成功", pOld)).build();
    }

    @DELETE
    @Path("/delete")
    @Operation(summary = "删除属性项")
    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('product:property:delete')")
    public Response deleteProperty(@QueryParam("id") Integer id) {
    	ProductProperty p = ProductProperty.findById(id);
    	if (p == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "该属性项不存在")).build();
		}
    	if(ProductPropertyValue.checkCountPropertyId(id)) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "属性项下存在属性值，无法删除")).build();
    	}
    	p.delete();
    	return Response.ok().entity(new ResultDataVo<>(200, "属性项删除成功")).build();
    }

    @GET
    @Path("/get")
    @Operation(summary = "获得属性项")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('product:property:query')")
    public Response getProperty(@QueryParam("id") Integer id) {
    	return Response.ok().entity(new ResultDataVo<>(200, ProductProperty.findById(id))).build();
    }

    @GET
    @Path("/list")
    @Operation(summary = "获得属性项列表")
//    @PreAuthorize("@ss.hasPermission('product:property:query')")
    public Response getPropertyList(@BeanParam ProductPropertyQuery query) {
    	List<ProductProperty> queryReturn = QueryHelper.createQueryReturn(em, ProductProperty.class, query).getResultList();
        return Response.ok().entity(new ResultDataVo<>(200, queryReturn)).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得属性项分页")
//    @PreAuthorize("@ss.hasPermission('product:property:query')")
    public Response getPropertyPage(@BeanParam ProductPropertyQuery query) {
    	VuePageListVo<ProductProperty> queryPage = QueryHelper.createQueryPage(em, ProductProperty.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, queryPage)).build();
    }

    @GET
    @Path("/get-value-list")
    @Operation(summary = "获得属性项列表")
//    @PreAuthorize("@ss.hasPermission('product:property:query')")
    public Response getPropertyAndValueList(@BeanParam ProductPropertyQuery query) {
    	List<ProductProperty> queryReturn = QueryHelper.createQueryReturn(em, ProductProperty.class, query).getResultList();
    	if (queryReturn == null || queryReturn.isEmpty()) {
    		return Response.ok().entity(new ResultDataVo<>(200, null)).build();
		}
    	List<Integer> ids = queryReturn.stream().map(ProductProperty::getId).collect(Collectors.toList());
    	List<ProductPropertyValue> values = ProductPropertyValue.list("propertyId in (?1)", ids);
    	Map<Integer, List<ProductPropertyValue>> pidMap = values.stream().collect(Collectors.groupingBy(ProductPropertyValue::getPropertyId));
    	for (ProductProperty p : queryReturn) {
			p.setValues(pidMap.get(p.getId()));
		}
        return Response.ok().entity(new ResultDataVo<>(200, queryReturn)).build();
    }

}
