package com.weir.quarkus.shop.vo;

/**
 * 
 * @ClassName: ResultVo
 * @Description: 返回数据格式
 * @author weir
 * @date 2021年8月28日
 *
 * @param <T>
 */
public class ResultDataVo<T> {

	public Integer code;
	public String message;
	public String type = "success";
	public T result;
	public ResultDataVo() {
	}
	public ResultDataVo(Integer code, String message) {
		this.code = code;
		this.message = message;
	}
	public ResultDataVo(Integer code, T result) {
		this.code = code;
		this.result = result;
	}
	public ResultDataVo(Integer code, String message, T result) {
		this.code = code;
		this.message = message;
		this.result = result;
	}
}
