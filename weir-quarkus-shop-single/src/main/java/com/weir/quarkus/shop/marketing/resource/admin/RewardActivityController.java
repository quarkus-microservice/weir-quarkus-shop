//package com.weir.quarkus.shop.marketing.resource.admin;
//
//
//import java.util.Arrays;
//import java.util.List;
//
//import org.apache.commons.lang3.StringUtils;
//import org.eclipse.microprofile.openapi.annotations.Operation;
//import org.eclipse.microprofile.openapi.annotations.tags.Tag;
//
//import com.weir.quarkus.shop.marketing.entity.RewardActivity;
//
//import jakarta.annotation.Resource;
//import jakarta.validation.Valid;
//import jakarta.ws.rs.POST;
//import jakarta.ws.rs.Path;
//import jakarta.ws.rs.core.Response;
//
//
//@Tag(name = "管理后台 - 满减送活动")
//@Path("/promotion/reward-activity")
//public class RewardActivityController {
//
//    @POST
//    @Path("/create")
//    @Operation(summary = "创建满减送活动")
////    @PreAuthorize("@ss.hasPermission('promotion:reward-activity:create')")
//    public Response createRewardActivity(RewardActivity rewardActivity) {
//    	String productSpuIds = rewardActivity.getProductSpuIds();
//    	if (StringUtils.isNotBlank(productSpuIds)) {
//    		List<String> spuIds = Arrays.asList(productSpuIds.split(","));
//    		RewardActivity
//		}
//    }
//
//    @PutMapping("/update")
//    @Operation(summary = "更新满减送活动")
//    @PreAuthorize("@ss.hasPermission('promotion:reward-activity:update')")
//    public CommonResult<Boolean> updateRewardActivity(@Valid @RequestBody RewardActivityUpdateReqVO updateReqVO) {
//        rewardActivityService.updateRewardActivity(updateReqVO);
//        return success(true);
//    }
//
//    @PutMapping("/close")
//    @Operation(summary = "关闭满减送活动")
//    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:reward-activity:close')")
//    public CommonResult<Boolean> closeRewardActivity(@RequestParam("id") Long id) {
//        rewardActivityService.closeRewardActivity(id);
//        return success(true);
//    }
//
//    @DeleteMapping("/delete")
//    @Operation(summary = "删除满减送活动")
//    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:reward-activity:delete')")
//    public CommonResult<Boolean> deleteRewardActivity(@RequestParam("id") Long id) {
//        rewardActivityService.deleteRewardActivity(id);
//        return success(true);
//    }
//
//    @GetMapping("/get")
//    @Operation(summary = "获得满减送活动")
//    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('promotion:reward-activity:query')")
//    public CommonResult<RewardActivityRespVO> getRewardActivity(@RequestParam("id") Long id) {
//        RewardActivity rewardActivity = rewardActivityService.getRewardActivity(id);
//        return success(RewardActivityConvert.INSTANCE.convert(rewardActivity));
//    }
//
//    @GetMapping("/page")
//    @Operation(summary = "获得满减送活动分页")
//    @PreAuthorize("@ss.hasPermission('promotion:reward-activity:query')")
//    public CommonResult<PageResult<RewardActivityRespVO>> getRewardActivityPage(@Valid RewardActivityPageReqVO pageVO) {
//        PageResult<RewardActivity> pageResult = rewardActivityService.getRewardActivityPage(pageVO);
//        return success(RewardActivityConvert.INSTANCE.convertPage(pageResult));
//    }
//
//}
