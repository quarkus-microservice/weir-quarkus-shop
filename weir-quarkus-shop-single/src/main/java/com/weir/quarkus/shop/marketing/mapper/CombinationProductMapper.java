package com.weir.quarkus.shop.marketing.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.weir.quarkus.shop.marketing.entity.CombinationActivity;
import com.weir.quarkus.shop.marketing.entity.CombinationProduct;
import com.weir.quarkus.shop.marketing.entity.CombinationRecord;
import com.weir.quarkus.shop.marketing.vo.CombinationActivityDTO;
import com.weir.quarkus.shop.marketing.vo.CombinationProductVO;
import com.weir.quarkus.shop.marketing.vo.CombinationRecordVO;

/** 
 * @Title: ProductBrandMapper.java 
 * @Package com.weir.quarkus.shop.product.mapper 
 * @Description: 收藏夹
 * @author weir
 * @date 2023年12月12日 14:12:05 
 * @version V1.0 
 */
@Mapper(componentModel = "jakarta", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CombinationProductMapper {

	CombinationActivity toEntity(CombinationActivityDTO dto);
	CombinationActivityDTO toDTO(CombinationActivity ca);
	List<CombinationActivityDTO> toDTO(List<CombinationActivity> cas);
	
	CombinationProductVO toVo(CombinationProduct cp);
	List<CombinationProductVO> toVo(List<CombinationProduct> cps);
	
	CombinationRecordVO toRecordVo(CombinationRecord cr);
	List<CombinationRecordVO> toRecordVo(List<CombinationRecord> crs);
}
