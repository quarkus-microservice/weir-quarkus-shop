package com.weir.quarkus.shop.marketing.vo;

import lombok.*;

/**
 * 砍价活动 DO
 *
 * @author HUIHUI
 */
@Data
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BargainActivityDTO {

    private Integer activityId;
    private Integer recordId;
    private Integer userCount;
   
}
