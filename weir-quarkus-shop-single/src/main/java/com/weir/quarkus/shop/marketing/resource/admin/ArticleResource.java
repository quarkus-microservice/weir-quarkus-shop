package com.weir.quarkus.shop.marketing.resource.admin;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.marketing.entity.Article;
import com.weir.quarkus.shop.marketing.entity.ArticleCategory;
import com.weir.quarkus.shop.marketing.vo.ArticleQuery;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Tag(name = "管理后台 - 文章管理")
@Path("/promotion/article")
public class ArticleResource {

	@Inject
	EntityManager em;
	
    @POST
    @Path("/create")
    @Operation(summary = "创建文章管理")
//    @PreAuthorize("@ss.hasPermission('promotion:article:create')")
    @Transactional
    public Response createArticle(@Valid @RequestBody Article article) {
    	ArticleCategory ac = ArticleCategory.findById(article.getCategoryId());
    	if (ac == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "文章分类ID不能为空")).build();
		}
    	article.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "创建文章成功" , article)).build();
    }

    @PUT
    @Path("/update")
    @Operation(summary = "更新文章管理")
//    @PreAuthorize("@ss.hasPermission('promotion:article:update')")
    @Transactional
    public Response updateArticle(@Valid @RequestBody Article article) {
    	ArticleCategory ac = ArticleCategory.findById(article.getCategoryId());
    	if (ac == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "文章分类ID不能为空")).build();
		}
    	article.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "更新文章成功" , article)).build();
    }

    @DELETE
    @Path("/delete")
    @Operation(summary = "删除文章管理")
    @Parameter(name = "id", description = "编号", required = true)
//    @PreAuthorize("@ss.hasPermission('promotion:article:delete')")
    public Response deleteArticle(@QueryParam("id") Integer id) {
        Article.deleteById(id);
        return Response.ok().entity(new ResultDataVo<>(200, "删除文章成功" , id)).build();
    }

    @GET
    @Path("/get")
    @Operation(summary = "获得文章管理")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
//    @PreAuthorize("@ss.hasPermission('promotion:article:query')")
    public Response getArticle(@QueryParam("id") Integer id) {
    	return Response.ok().entity(new ResultDataVo<>(200, "文章成功" , Article.findById(id))).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得文章管理分页")
//    @PreAuthorize("@ss.hasPermission('promotion:article:query')")
    public Response getArticlePage(@Valid ArticleQuery query) {
    	VuePageListVo<Article> queryPage = QueryHelper.createQueryPage(em, Article.class, query);
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPage)).build();
    }

}
