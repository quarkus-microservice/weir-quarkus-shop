package com.weir.quarkus.shop.product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.*;

import java.util.Date;

import com.weir.quarkus.shop.entity.BaseEntity;

/**
 * 
 * @Title: ProductComment.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品评论
 * @author weir
 * @date 2023年12月12日 10:05:21 
 * @version V1.0
 */
@Entity
@Table(name = "product_comment")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductComment extends BaseEntity {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 评价人的用户编号
     *
     */
    @Column(name = "user_id")
    private Integer userId;
    /**
     * 评价人名称
     */
    @Column(name = "user_nickname")
    private String userNickname;
    /**
     * 评价人头像
     */
    @Column(name = "user_avatar")
    private String userAvatar;
    /**
     * 是否匿名
     */
    private Boolean anonymous;

    /**
     * 交易订单编号
     *
     */
    @Column(name = "order_id")
    private Integer orderId;
    /**
     * 交易订单项编号
     *
     */
    @Column(name = "order_item_id")
    private Integer orderItemId;

    /**
     * 商品 SPU 编号
     *
     */
    @Column(name = "spu_id")
    private Integer spuId;
    /**
     * 商品 SPU 名称
     *
     */
    @Column(name = "spu_name")
    private String spuName;
    /**
     * 商品 SKU 编号
     *
     */
    @Column(name = "sku_id")
    private Integer skuId;
    /**
     * 商品 SKU 图片地址
     *
     */
    @Column(name = "sku_pic_url")
    private String skuPicUrl;
    /**
     * 属性数组，JSON 格式
     *
     */
    @Column(name = "sku_properties")
    private String skuProperties;

    /**
     * 是否可见
     *
     * true:显示
     * false:隐藏
     */
    private Boolean visible;
    /**
     * 评分星级
     *
     * 1-5 分
     */
    private Integer scores;
    /**
     * 描述星级
     *
     * 1-5 星
     */
    @Column(name = "description_scores")
    private Integer descriptionScores;
    /**
     * 服务星级
     *
     * 1-5 星
     */
    @Column(name = "benefit_scores")
    private Integer benefitScores;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 评论图片地址数组（逗号分隔）
     */
    @Column(name = "pic_urls")
    private String picUrls;

    /**
     * 商家是否回复
     */
    @Column(name = "reply_status")
    private Boolean replyStatus;
    /**
     * 回复管理员编号
     */
    @Column(name = "reply_user_id")
    private Integer replyUserId;
    /**
     * 商家回复内容
     */
    @Column(name = "reply_content")
    private String replyContent;
    /**
     * 商家回复时间
     */
    @Column(name = "reply_time")
    private Date replyTime;

}
