package com.weir.quarkus.shop.marketing.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.shop.marketing.entity.CombinationActivity;
import com.weir.quarkus.shop.marketing.entity.CombinationProduct;
import com.weir.quarkus.shop.marketing.entity.CombinationRecord;
import com.weir.quarkus.shop.marketing.entity.QCombinationRecord;
import com.weir.quarkus.shop.marketing.mapper.CombinationProductMapper;
import com.weir.quarkus.shop.marketing.vo.CombinationActivityDTO;
import com.weir.quarkus.shop.marketing.vo.CombinationProductVO;
import com.weir.quarkus.shop.marketing.vo.CombinationRecordQuery;
import com.weir.quarkus.shop.marketing.vo.CombinationRecordSummaryVO;
import com.weir.quarkus.shop.marketing.vo.CombinationRecordVO;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 拼团记录")
@Path("/promotion/combination-record")
public class CombinationRecordResource {

	private static final QCombinationRecord qCombinationRecord = QCombinationRecord.combinationRecord;

	@Inject
	EntityManager em;
	@Inject
	CombinationProductMapper combinationProductMapper;
	@Inject
	JPAQueryFactory jpaQueryFactory;

	@GET
	@Path("/page")
	@Operation(summary = "获得拼团记录分页")
//    @PreAuthorize("@ss.hasPermission('promotion:combination-record:query')")
	public Response getCombinationRecordPage(@Valid CombinationRecordQuery query) {
		VuePageListVo<CombinationRecord> queryPage = QueryHelper.createQueryPage(em, CombinationRecord.class, query);
		List<CombinationRecord> list = queryPage.getList();
		List<CombinationRecordVO> recordVos = combinationProductMapper.toRecordVo(list);
		List<Integer> activityIds = recordVos.stream().map(CombinationRecordVO::getActivityId).distinct()
				.collect(Collectors.toList());
		List<CombinationActivity> combinationActivities = CombinationActivity.list("id in (?1)", activityIds);
		List<CombinationActivityDTO> dtos = combinationProductMapper.toDTO(combinationActivities);
		Map<Integer, CombinationActivityDTO> combinationActivityDTOMap = dtos.stream()
				.collect(Collectors.toMap(CombinationActivityDTO::getId, a -> a, (k1, k2) -> k1));
		List<CombinationProduct> combinationProducts = CombinationProduct.list("activityId in (?1)", activityIds);
		List<CombinationProductVO> combinationProductVOs = combinationProductMapper.toVo(combinationProducts);
		Map<Integer, List<CombinationProductVO>> combinationProductVOMap = combinationProductVOs.stream()
				.collect(Collectors.groupingBy(CombinationProductVO::getActivityId));
		recordVos.forEach(c -> {
			CombinationActivityDTO combinationActivityDTO = combinationActivityDTOMap.get(c.getActivityId());
			if (combinationActivityDTO != null) {
				List<CombinationProductVO> productVOs = combinationProductVOMap.get(c.getActivityId());
				combinationActivityDTO.setProducts(productVOs);
				c.setActivityDTO(combinationActivityDTO);
			}
		});
		VuePageListVo<CombinationRecordVO> queryPageVo = new VuePageListVo<>(recordVos, queryPage.getPage(),
				queryPage.getPageSize(), queryPage.getPageCount(), queryPage.getTotal());
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPageVo)).build();
	}

	@GET
	@Path("/get-summary")
	@Operation(summary = "获得拼团记录的概要信息", description = "用于拼团记录页面展示")
//    @PreAuthorize("@ss.hasPermission('promotion:combination-record:query')")
	public Response getCombinationRecordSummary() {
		CombinationRecordSummaryVO summaryVO = new CombinationRecordSummaryVO();
		List<Long> fetch = jpaQueryFactory.select(qCombinationRecord.userId.countDistinct()).from(qCombinationRecord)
				.fetch();
		long successCount = CombinationRecord.count("status = 0 and headId = 0");
		long virtualGroupCount = CombinationRecord.count("virtualGroup = 1 and headId = 0");
		summaryVO.setSuccessCount(successCount);
		summaryVO.setVirtualGroupCount(virtualGroupCount);
		return Response.ok().entity(new ResultDataVo<>(200, summaryVO)).build();
	}

}
