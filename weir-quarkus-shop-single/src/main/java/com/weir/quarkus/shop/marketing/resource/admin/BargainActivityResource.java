package com.weir.quarkus.shop.marketing.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.weir.quarkus.shop.marketing.entity.BargainActivity;
import com.weir.quarkus.shop.marketing.entity.QBargainHelp;
import com.weir.quarkus.shop.marketing.entity.QBargainRecord;
import com.weir.quarkus.shop.marketing.mapper.BargainActivityMapper;
import com.weir.quarkus.shop.marketing.vo.BargainActivityDTO;
import com.weir.quarkus.shop.marketing.vo.BargainActivityQuery;
import com.weir.quarkus.shop.marketing.vo.BargainActivityVO;
import com.weir.quarkus.shop.product.entity.ProductSku;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 砍价活动")
@Path("/promotion/bargain-activity")
public class BargainActivityResource {
	private static final QBargainRecord qBargainRecord = QBargainRecord.bargainRecord;
	private static final QBargainHelp qBargainHelp = QBargainHelp.bargainHelp;

	@Inject BargainActivityMapper bargainActivityMapper;
	@Inject EntityManager em;
	@Inject JPAQueryFactory jpaQueryFactory;
	
	
    @POST
    @Path("/create")
    @Operation(summary = "创建砍价活动")
    @Transactional
//    @PreAuthorize("@ss.hasPermission('promotion:bargain-activity:create')")
    public Response createBargainActivity(@Valid @RequestBody BargainActivity bargainActivity) {
    	ProductSpu spu = ProductSpu.findById(bargainActivity.getSpuId());
    	if (spu == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SPU 不存在")).build();
		}
    	ProductSku sku = ProductSku.findById(bargainActivity.getSkuId());
    	if (sku == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SKU 不存在")).build();
		}
    	long count = BargainActivity.count("status = 0 and spuId=?1", bargainActivity.getSpuId());
    	// 存在商品参加了其它砍价活动
    	if (count > 0) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "存在商品参加了其它砍价活动")).build();
		}
    	bargainActivity.persist();
    	return Response.ok().entity(new ResultDataVo<>(200, "创建砍价活动成功" , bargainActivity)).build();
    }

    @PUT
    @Path("/update")
    @Operation(summary = "更新砍价活动")
    @Transactional
//    @PreAuthorize("@ss.hasPermission('promotion:bargain-activity:update')")
    public Response updateBargainActivity(@Valid @RequestBody BargainActivity bargainActivity) {
    	BargainActivity old = BargainActivity.findById(bargainActivity.getId());
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "砍价活动不存在")).build();
		}
        if (old.getStatus() == 1) {
        	return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "砍价活动已关闭，不能修改")).build();
		}
        ProductSpu spu = ProductSpu.findById(bargainActivity.getSpuId());
    	if (spu == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SPU 不存在")).build();
		}
    	ProductSku sku = ProductSku.findById(bargainActivity.getSkuId());
    	if (sku == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "商品 SKU 不存在")).build();
		}
    	
        List<BargainActivity> list = BargainActivity.list("status = 0 and spuId=?1", bargainActivity.getSpuId());
        if (CollectionUtils.isNotEmpty(list)) {
			if (list.size() == 1) {
				if (!list.get(0).getId().equals(bargainActivity.getId())) {
					return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "存在商品参加了其它砍价活动")).build();
				}
			}else {
				return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "存在商品参加了其它砍价活动")).build();
			}
		}
        bargainActivity.persist();
        return Response.ok().entity(new ResultDataVo<>(200, "更新砍价活动成功" , bargainActivity)).build();
    }

    @PUT
    @Path("/close")
    @Operation(summary = "关闭砍价活动")
    @Parameter(name = "id", description = "编号", required = true)
    @Transactional
//    @PreAuthorize("@ss.hasPermission('promotion:bargain-activity:close')")
    public Response closeSeckillActivity(@QueryParam("id") Integer id) {
    	BargainActivity old = BargainActivity.findById(id);
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "砍价活动不存在")).build();
		}
    	if (old.getStatus() == 1) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "砍价活动已关闭")).build();
		}
    	old.setStatus(1);
    	return Response.ok().entity(new ResultDataVo<>(200, "关闭砍价活动成功" , old)).build();
    }

    @DELETE
    @Path("/delete")
    @Operation(summary = "删除砍价活动")
    @Parameter(name = "id", description = "编号", required = true)
    @Transactional
//    @PreAuthorize("@ss.hasPermission('promotion:bargain-activity:delete')")
    public Response deleteBargainActivity(@QueryParam("id") Integer id) {
    	BargainActivity old = BargainActivity.findById(id);
    	if (old == null) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "砍价活动不存在")).build();
		}
    	if (old.getStatus() == 0) {
    		return Response.status(Status.BAD_REQUEST).entity(new ResultDataVo<>(400, "砍价活动未关闭或未结束，不能删除")).build();
		}
    	BargainActivity.deleteById(id);
    	return Response.ok().entity(new ResultDataVo<>(200, "删除砍价活动成功" , id)).build();
    }

    @GET
    @Path("/get")
    @Operation(summary = "获得砍价活动")
    @Parameter(name = "id", description = "编号", required = true, example = "1024")
    @Transactional
//    @PreAuthorize("@ss.hasPermission('promotion:bargain-activity:query')")
    public Response getBargainActivity(@QueryParam("id") Integer id) {
    	return Response.ok().entity(new ResultDataVo<>(200, BargainActivity.findById(id))).build();
    }

    @GET
    @Path("/page")
    @Operation(summary = "获得砍价活动分页")
//    @PreAuthorize("@ss.hasPermission('promotion:bargain-activity:query')")
    public Response getBargainActivityPage(@Valid BargainActivityQuery query) {
    	VuePageListVo<BargainActivity> queryPage = QueryHelper.createQueryPage(em, BargainActivity.class, query);
    	List<BargainActivity> list = queryPage.getList();
    	List<BargainActivityVO> bargainActivityVOs = bargainActivityMapper.toVo(list);
    	List<Integer> ids = list.stream().map(BargainActivity::getId).distinct().collect(Collectors.toList());
    	List<Integer> spuIds = list.stream().map(BargainActivity::getSpuId).distinct().collect(Collectors.toList());
    	List<ProductSpu> spus = ProductSpu.list("id in (?1)", spuIds);
    	Map<Integer, ProductSpu> spuMap = spus.stream().collect(Collectors.toMap(ProductSpu::getId, a -> a, (k1, k2) -> k1));
    	
    	List<BargainActivityDTO> activityIdUserCountList = jpaQueryFactory.select(
    			Projections.fields(BargainActivityDTO.class,
    			qBargainRecord.userId.countDistinct().as("userCount"),qBargainRecord.activityId))
    	.from(qBargainRecord).where(qBargainRecord.activityId.in(ids)).groupBy(qBargainRecord.activityId).fetch();
    	Map<Integer, BargainActivityDTO> activityIdUserCountMap = activityIdUserCountList.stream().collect(Collectors.toMap(BargainActivityDTO::getActivityId, a -> a, (k1, k2) -> k1));
    	List<BargainActivityDTO> activityIdUserCountSuccessList = jpaQueryFactory.select(
    			Projections.fields(BargainActivityDTO.class,
    					qBargainRecord.userId.countDistinct().as("userCount"),qBargainRecord.activityId))
    			.from(qBargainRecord).where(qBargainRecord.activityId.in(ids).and(qBargainRecord.status.eq(2)))
    			.groupBy(qBargainRecord.activityId).fetch();
    	Map<Integer, BargainActivityDTO> activityIdUserCountSuccessMap = activityIdUserCountSuccessList.stream().collect(Collectors.toMap(BargainActivityDTO::getActivityId, a -> a, (k1, k2) -> k1));
    	List<BargainActivityDTO> activityIdUserCountHelpList = jpaQueryFactory.select(
    			Projections.fields(BargainActivityDTO.class,
    					qBargainHelp.userId.countDistinct().as("userCount"),qBargainHelp.activityId))
    			.from(qBargainHelp).where(qBargainHelp.activityId.in(ids))
    			.groupBy(qBargainHelp.activityId).fetch();
    	Map<Integer, BargainActivityDTO> activityIdUserCountHelpMap = activityIdUserCountHelpList.stream().collect(Collectors.toMap(BargainActivityDTO::getActivityId, a -> a, (k1, k2) -> k1));
    	
    	bargainActivityVOs.forEach(b->{
    		ProductSpu productSpu = spuMap.get(b.getSpuId());
    		if (productSpu != null) {
				b.setSpuName(productSpu.getName());
				b.setPicUrl(productSpu.getPicUrl());
			}
    		BargainActivityDTO activityIdUserCount = activityIdUserCountMap.get(b.getId());
    		if (activityIdUserCount != null) {
				b.setRecordUserCount(activityIdUserCount.getUserCount());
			}
    		BargainActivityDTO activityIdUserCountSuccess = activityIdUserCountSuccessMap.get(b.getId());
    		if (activityIdUserCountSuccess != null) {
    			b.setRecordSuccessUserCount(activityIdUserCountSuccess.getUserCount());
    		}
    		BargainActivityDTO activityIdUserCountHelp = activityIdUserCountHelpMap.get(b.getId());
    		if (activityIdUserCountHelp != null) {
    			b.setHelpUserCount(activityIdUserCountHelp.getUserCount());
    		}
    	});
    	VuePageListVo<BargainActivityVO> queryPageVo = new VuePageListVo<>(bargainActivityVOs,queryPage.getPage(),queryPage.getPageSize(),queryPage.getPageCount(),queryPage.getTotal());
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPageVo)).build();
    }

}
