package com.weir.quarkus.shop.product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import com.weir.quarkus.shop.entity.BaseEntity;

import lombok.*;

/**
 * 商品 SKU
 * @Title: ProductSku.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品 SKU
 * @author weir
 * @date 2023年12月13日 15:44:08 
 * @version V1.0
 */
@Entity
@Table(name = "product_sku")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductSku extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * SPU 编号
     *
     */
	@Column(name = "spu_id")
    private Integer spuId;
    /**
     * 商品价格，单位：分
     */
    private Integer price;
    /**
     * 市场价，单位：分
     */
    @Column(name = "market_price")
    private Integer marketPrice;
    /**
     * 成本价，单位：分
     */
    @Column(name = "cost_price")
    private Integer costPrice;
    /**
     * 商品条码
     */
    @Column(name = "bar_code")
    private String barCode;
    /**
     * 图片地址
     */
    @Column(name = "pic_url")
    private String picUrl;
    /**
     * 库存
     */
    private Integer stock;
    /**
     * 商品重量，单位：kg 千克
     */
    private Double weight;
    /**
     * 商品体积，单位：m^3 平米
     */
    private Double volume;

    /**
     * 一级分销的佣金，单位：分
     */
    @Column(name = "first_brokerage_price")
    private Integer firstBrokeragePrice;
    /**
     * 二级分销的佣金，单位：分
     */
    @Column(name = "second_brokerage_price")
    private Integer secondBrokeragePrice;

    /**
     * 商品销量
     */
    @Column(name = "sales_count")
    private Integer salesCount;

}

