package com.weir.quarkus.shop.marketing.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import lombok.*;

import java.time.LocalDateTime;

import com.weir.quarkus.shop.entity.BaseEntity;

/**
 * 拼团商品 DO
 *
 * @author HUIHUI
 */
@Entity
@Table(name = "promotion_combination_product")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CombinationProduct extends BaseEntity {

    /**
     * 编号
     */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 拼团活动编号
     */
    private Integer activityId;
    /**
     * 商品 SPU 编号
     */
    private Integer spuId;
    /**
     * 商品 SKU 编号
     */
    private Integer skuId;
    /**
     * 拼团价格，单位分
     */
    private Integer combinationPrice;

    /**
     * 拼团商品状态
     *
     * 关联 {@link CombinationActivity#getStatus()}
     */
    private Integer activityStatus;
    /**
     * 活动开始时间点
     *
     * 冗余 {@link CombinationActivity#getStartTime()}
     */
    private LocalDateTime activityStartTime;
    /**
     * 活动结束时间点
     *
     * 冗余 {@link CombinationActivity#getEndTime()}
     */
    private LocalDateTime activityEndTime;

}
