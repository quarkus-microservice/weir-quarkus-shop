package com.weir.quarkus.shop.product.vo.app;

import java.util.List;

import org.eclipse.microprofile.openapi.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Schema(description = "用户 APP - 删除商品浏览记录的 Request VO")
@Data
public class AppProductBrowseHistoryDeleteReqVO {
    @Schema(description = "商品 SPU 编号数组", example = "29502")
    @NotEmpty(message = "商品 SPU 编号数组不能为空")
    private List<Long> spuIds;

}