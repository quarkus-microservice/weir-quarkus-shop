package com.weir.quarkus.shop.product.resource.app;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.product.entity.ProductCategory;
import com.weir.quarkus.shop.vo.ResultDataVo;

import io.quarkus.panache.common.Sort;

@Tag(name = "用户 APP - 商品分类")
@Path("app/product/category")
public class AppCategoryResource {

    @GET
    @Path("/list")
    @Operation(summary = "获得商品分类列表")
    public Response getProductCategoryList() {
    	List<ProductCategory> list = ProductCategory.list("status", Sort.by("sort"),0);
        return Response.ok().entity(new ResultDataVo<>(200, "ok", list)).build();
    }

    @GET
    @Path("/list-by-ids")
    @Operation(summary = "获得商品分类列表，指定编号")
    @Parameter(name = "ids", description = "商品分类编号数组", required = true)
    public Response getProductCategoryList(@QueryParam("ids") List<Long> ids) {
        if (CollectionUtils.isEmpty(ids)) {
            return Response.ok().entity(new ResultDataVo<>(200, "ok", new ArrayList<>())).build();
        }
        List<ProductCategory> list = ProductCategory.list("id in (?1) and status = ?2", Sort.by("sort"),ids,0);
        return Response.ok().entity(new ResultDataVo<>(200, "ok", list)).build();
    }

}
