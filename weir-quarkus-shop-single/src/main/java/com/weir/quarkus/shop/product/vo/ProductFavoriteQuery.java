package com.weir.quarkus.shop.product.vo;

import lombok.*;

import com.weir.quarkus.shop.vo.BaseQuery;

/**
 * 
 * @Title: ProductComment.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品评论
 * @author weir
 * @date 2023年12月12日 10:05:21 
 * @version V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductFavoriteQuery extends BaseQuery {

    private Integer userId;

}
