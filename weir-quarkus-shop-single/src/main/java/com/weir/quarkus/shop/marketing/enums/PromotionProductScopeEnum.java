package com.weir.quarkus.shop.marketing.enums;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PromotionProductScopeEnum {

    ALL(1, "通用券"), // 全部商品
    SPU(2, "商品券"), // 指定商品
    CATEGORY(3, "品类券"), // 指定品类
    ;

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(PromotionProductScopeEnum::getScope).toArray();

    /**
     * 范围值
     */
    private final Integer scope;
    /**
     * 范围名
     */
    private final String name;

    public int[] array() {
        return ARRAYS;
    }

}