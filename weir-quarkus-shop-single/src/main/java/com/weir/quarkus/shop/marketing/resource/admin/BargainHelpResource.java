package com.weir.quarkus.shop.marketing.resource.admin;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import com.weir.quarkus.shop.marketing.entity.BargainHelp;
import com.weir.quarkus.shop.marketing.mapper.BargainHelpMapper;
import com.weir.quarkus.shop.marketing.vo.BargainHelpQuery;
import com.weir.quarkus.shop.marketing.vo.BargainHelpVO;
import com.weir.quarkus.shop.member.entity.MemberUser;
import com.weir.quarkus.shop.utils.QueryHelper;
import com.weir.quarkus.shop.vo.ResultDataVo;
import com.weir.quarkus.shop.vo.VuePageListVo;

@Tag(name = "管理后台 - 砍价助力")
@Path("/promotion/bargain-help")
public class BargainHelpResource {

	@Inject EntityManager em;
	@Inject BargainHelpMapper bargainHelpMapper;
	
    @GET
    @Path("/page")
    @Operation(summary = "获得砍价助力分页")
//    @PreAuthorize("@ss.hasPermission('promotion:bargain-help:query')")
    public Response getBargainHelpPage(@Valid BargainHelpQuery query) {
    	VuePageListVo<BargainHelp> queryPage = QueryHelper.createQueryPage(em, BargainHelp.class, query);
    	List<BargainHelp> list = queryPage.getList();
    	List<Integer> userIds = list.stream().map(BargainHelp::getUserId).distinct().collect(Collectors.toList());
    	List<MemberUser> memberUsers = MemberUser.list("id in (?1)", userIds);
    	Map<Integer, MemberUser> memberUserMap = memberUsers.stream().collect(Collectors.toMap(MemberUser::getId, a -> a, (k1, k2) -> k1));
    	
    	List<BargainHelpVO> helpVOs = bargainHelpMapper.toVo(list);
    	helpVOs.forEach(h->{
    		MemberUser memberUser = memberUserMap.get(h.getUserId());
    		if (memberUser != null) {
				h.setNickname(memberUser.getNickname());
				h.setAvatar(memberUser.getAvatar());
			}
    	});
    	VuePageListVo<BargainHelpVO> queryPageVo = new VuePageListVo<>(helpVOs,queryPage.getPage(),queryPage.getPageSize(),queryPage.getPageCount(),queryPage.getTotal());
		return Response.ok().entity(new ResultDataVo<>(200, "ok", queryPageVo)).build();
    }

}
