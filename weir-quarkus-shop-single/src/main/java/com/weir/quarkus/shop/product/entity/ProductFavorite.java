package com.weir.quarkus.shop.product.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import com.weir.quarkus.shop.entity.BaseEntity;

import lombok.*;

/**
 * 
 * @Title: ProductFavorite.java 
 * @Package com.weir.quarkus.shop.product.entity 
 * @Description: 商品收藏
 * @author weir
 * @date 2023年12月12日 10:08:28 
 * @version V1.0
 */
@Entity
@Table(name = "product_favorite")
@Data
@EqualsAndHashCode(callSuper = true)
public class ProductFavorite extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 用户编号
     *
     */
	@Column(name = "user_id")
    private Integer userId;
    /**
     * 商品 SPU 编号
     *
     */
	@Column(name = "spu_id")
    private Integer spuId;

}
