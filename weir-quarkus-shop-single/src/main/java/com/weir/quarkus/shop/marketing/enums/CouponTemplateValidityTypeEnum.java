package com.weir.quarkus.shop.marketing.enums;

import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 优惠劵模板的有限期类型的枚举
 *
 * @author 芋道源码
 */
@AllArgsConstructor
@Getter
public enum CouponTemplateValidityTypeEnum {

    DATE(1, "固定日期"),
    TERM(2, "领取之后"),
    ;

    public static final int[] ARRAYS = Arrays.stream(values()).mapToInt(CouponTemplateValidityTypeEnum::getType).toArray();

    /**
     * 值
     */
    private final Integer type;
    /**
     * 名字
     */
    private final String name;

    public int[] array() {
        return ARRAYS;
    }

}