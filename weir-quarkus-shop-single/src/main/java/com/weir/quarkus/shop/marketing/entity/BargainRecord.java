package com.weir.quarkus.shop.marketing.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

import com.weir.quarkus.shop.entity.BaseEntity;

import lombok.*;

import java.time.LocalDateTime;

/**
 * 砍价记录 DO TODO
 *
 * @author HUIHUI
 */
@Entity
@Table(name = "promotion_bargain_record")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BargainRecord extends BaseEntity {

    /**
     * 编号
     */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    /**
     * 用户编号
     */
    private Integer userId;

    /**
     * 砍价活动编号
     *
     * 关联 {@link BargainActivity#getId()} 字段
     */
    private Integer activityId;
    /**
     * 商品 SPU 编号
     */
    private Integer spuId;
    /**
     * 商品 SKU 编号
     */
    private Integer skuId;

    /**
     * 砍价起始价格，单位：分
     */
    private Integer bargainFirstPrice;
    /**
     * 当前砍价，单位：分
     */
    private Integer bargainPrice;

    /**
     * 砍价状态
     *
     * 砍价成功的条件是：（2 选 1）
     *  1. 砍价到 {@link BargainActivity#getBargainMinPrice()} 底价
     *  2. 助力人数到达 {@link BargainActivity#getHelpMaxCount()} 人
     *
     * 枚举 {@link BargainRecordStatusEnum}
     */
    private Integer status;
    /**
     * 结束时间
     */
    private LocalDateTime endTime;

    /**
     * 订单编号
     */
    private Integer orderId;

}
