package com.weir.quarkus.shop.product.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import com.weir.quarkus.shop.product.entity.ProductSku;
import com.weir.quarkus.shop.product.entity.ProductSpu;
import com.weir.quarkus.shop.product.vo.ProductSkuVo;
import com.weir.quarkus.shop.product.vo.ProductSpuVo;

/** 
 * @Title: ProductBrandMapper.java 
 * @Package com.weir.quarkus.shop.product.mapper 
 * @Description: 收藏夹
 * @author weir
 * @date 2023年12月12日 14:12:05 
 * @version V1.0 
 */
@Mapper(componentModel = "jakarta", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ProductSpuMapper {

	ProductSpu toSpuEntity(ProductSpuVo spu);
	ProductSku toSkuEntity(ProductSkuVo sku);
	ProductSpuVo toSpuVo(ProductSpu spu);
	ProductSkuVo toSkuVo(ProductSku sku);
	List<ProductSkuVo> toSkuVo(List<ProductSku> skus);
	List<ProductSpuVo> toSpuVo(List<ProductSpu> spus);
}
